@extends('layouts.app')

@section('content')
  @section('masthead')
    @include('partials.masthead')
  @endsection
@if ($featured_post)
@include('partials/featured-post')
@endif
@include('partials/categories-filter')
<div class="container">
    <div class="row justify-center">
        <div class="column xs-100 lg-75">
            @if (!have_posts())
                <div class="alert alert-warning">
                {{ __('Sorry, no results were found.', 'visceral') }}
                </div>
                {!! get_search_form(false) !!}
            @endif
            <div class="posts-list">
                <div class="row">
                    @while (have_posts()) @php(the_post())
                        @include('partials.list-item-'.get_post_type())
                    @endwhile
                </div>
            </div>
            <div class="row">
                <div class="column xs-100 lg-25"></div>
                <div class="column xs-100 lg-75">{!! get_the_posts_navigation() !!}</div>
            </div>
            
        </div>
    </div>   
</div>
@endsection
