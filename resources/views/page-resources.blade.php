@extends('layouts.app')
@section('content')
<div class="container">
    @while(have_posts()) @php(the_post())
    @section('masthead')
    @include('partials.masthead')
    @endsection
    @include('partials.content-page')
    <div class="row justify-center">
        <div class="column xs-100 lg-83">
            <div class="row">
                <div class="column xs-100 lg-30 sidebar-nav-container">
                    @if (!empty($resource_topics))
                    <div class="taxonomy-filter sidebar-nav">
                        <form class="taxonomy-filter__form" action="#resource-list">
                            <label for="resource_search" class="screen-reader-text">Search</label>
                            <input type="text" name="resource_search" id="resource_search"
                                value="{{$_REQUEST['resource_search'] ? $_REQUEST['resource_search'] : ''}}">
                            <button type="submit"><i class="icon-search" aria-hidden="true"></i><span
                                    class="screen-reader-text">Submit</span></button>
                        </form>
                        <p class="taxonomy-filter__heading h6"><i class="icon icon-filter" aria-hidden="true"></i>Filter
                            Topics</p>
                        <ul class="taxonomy-filter__list">
                            <li class="taxonomy-filter__item">
                                <a href="{{get_permalink()}}"
                                    class="taxonomy-filter__link {{$_REQUEST['topic'] === '' || is_null($_REQUEST['topic']) ? 'taxonomy-filter__link--active' : ''}}">All<i
                                        class="icon-chevron-right" aria-hidden="true"></i></a>
                            </li>
                            @foreach ($resource_topics as $topic)
                            <li
                                class="taxonomy-filter__item {{ ($topic->parent != 0) ? 'taxonomy-filter__item--child' : ''}}">
                                <a href="{{get_permalink()}}?topic={{$topic->slug}}#resource-list"
                                    class="taxonomy-filter__link {{$_REQUEST['topic'] === $topic->slug ? 'taxonomy-filter__link--active' : ''}}">{{$topic->name}}<i
                                        class="icon-chevron-right" aria-hidden="true"></i></a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                <div class="column xs-100 lg-70">
                    <div id="resource-list" class="resource-list">
                        <div class="row">
                            @if ($resources->posts)
                            @foreach ($resources->posts as $resource)
                            @include('partials/list-item-resource', $resource)
                            @endforeach
                            @else
                            <div class="column xs-100">
                                <p>{{__('No resources found. Please try another search or filter option.', 'visceral')}}
                                </p>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="back-to-top">
        <a href="#main-content">
            <svg width="43px" height="43px" viewBox="0 0 43 43" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
                <!-- Generator: Sketch 62 (91390) - https://sketch.com -->
                <title>Group 7</title>
                <desc>Created with Sketch.</desc>
                <g id="Style-Guide" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Buttons-&amp;-Components" transform="translate(-1107.000000, -190.000000)">
                        <g id="Group-7" transform="translate(1107.000000, 190.000000)">
                            <circle id="Oval-Copy-5" fill="#00334D"
                                transform="translate(21.500000, 21.500000) rotate(-180.000000) translate(-21.500000, -21.500000) "
                                cx="21.5" cy="21.5" r="21.5"></circle>
                            <path
                                d="M15.6187762,17.1134648 C16.0757548,16.6592242 16.7956632,16.6310129 17.2853141,17.0273494 L17.3865352,17.1187762 L21.475,21.231 L25.6187603,17.1134807 C26.0757307,16.6592318 26.7956386,16.6310076 27.2852966,17.0273353 L27.3865194,17.1187603 C27.8407683,17.5757307 27.8689924,18.2956387 27.4726648,18.7852967 L27.3812397,18.8865194 L21.4647485,24.7677749 L15.6134648,18.8812238 C15.1267784,18.391604 15.1291563,17.6001513 15.6187762,17.1134648 Z"
                                id="Combined-Shape-Copy" fill="#FFFFFF" fill-rule="nonzero"
                                transform="translate(21.500000, 20.758887) scale(-1, -1) translate(-21.500000, -20.758887) ">
                            </path>
                        </g>
                    </g>
                </g>
            </svg>
        </a>
    </div>
    @endwhile
</div>
@endsection