@php
/**
* Masthead
* Default masthead template
*
* To override on specifc pages or posts, use
* separate partials ('masthead-single') and
* include on their relative blade partial
*
*/
@endphp

<div class="masthead {{ $masthead_image->img_class }}"
    data-image-src="{{ is_page_template('views/template-top-level.blade.php') || is_front_page() || is_page('resources') ? $masthead_image->full_size_image_url : ''}}">
    <?php // The low quality placeholder
    if (is_page_template('views/template-top-level.blade.php') || is_front_page() || is_page('resources')) :
        if ( $masthead_image->placeholder_image_url != '' ) : ?>
    <span class="masthead__overlay img-bg"
        style="background-image: url({{$masthead_image->placeholder_image_url}});"></span>
    <?php endif; ?>
    <?php endif; ?>

    <div class="container">
        <div class="row justify-center">
            <div class="column xs-100 {{ is_front_page() ? '' : 'lg-83'}}">
                <div class="masthead__content">
                    @if (!is_front_page() && !is_search() && !is_home())
                    @if (function_exists('bcn_display'))
                    @if (is_page())
                    @if (!App\is_top_level_page(get_the_id()))
                    <p class="masthead__breadcrumbs breadcrumbs reveal reveal-up">{{ bcn_display() }}</p>
                    @endif
                    @else
                    <p class="masthead__breadcrumbs breadcrumbs reveal reveal-up">{{ bcn_display() }}</p>
                    @endif
                    @endif
                    @endif
                    @if ($alternative_title && !is_search())
                    @if (!is_front_page())
                    @if (App\is_top_level_page(get_the_id()))
                    <h1 class="page-title--small reveal reveal-up">{!! App::title() !!}</h1>
                    <p class="page-title reveal reveal-up">{{$alternative_title}}</p>
                    @else
                    <h1 class="page-title reveal reveal-up">{{$alternative_title}}</h1>
                    @endif
                    @else
                    <h1 class="page-title reveal reveal-up">{{$alternative_title}}</h1>
                    @endif
                    @else
                    <h1 class="page-title reveal reveal-up">{!! App::title() !!}</h1>
                    @endif
                    @if (isset($post_fields['job_title']))
                    <p class="single-person__job-title reveal reveal-up">{{$post_fields['job_title']}}</p>
                    @endif
                    @if (isset($post_fields['masthead_content']))
                    <div class="masthead__content reveal reveal-up">{!! $post_fields['masthead_content'] !!}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if (!is_page('resources') && !is_home() && !is_front_page() && !is_search() &&
!is_page_template('views/template-top-level.blade.php'))
@if ($masthead_image->full_size_image_url)
<div class="masthead__image reveal reveal-up">
    <div class="img-cover">
        <img class="" src="{{$masthead_image->full_size_image_url}}" alt="">
    </div>
</div>
@endif

@else
@if (!is_home() && !is_front_page() && !is_search())
<a href="#main-content" class="masthead__down-arrow" aria-label="Go to main content">
    <svg width="13px" height="19px" viewBox="0 0 13 19" version="1.1" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Style-Guide" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
            <g id="Buttons-&amp;-Components" transform="translate(-948.000000, -203.000000)" stroke="#FFFFFF"
                stroke-width="2.5">
                <g id="Group-3" transform="translate(933.000000, 190.000000)">
                    <g id="Group-7" transform="translate(16.500000, 15.000000)">
                        <path d="M5,0 L5,13" id="Path-3"></path>
                        <polyline id="Path-4" points="2.77555756e-16 10 4.97004406 15 10 10"></polyline>
                    </g>
                </g>
            </g>
        </g>
    </svg>
</a>
@endif
@endif