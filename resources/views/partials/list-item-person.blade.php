<div class="column xs-100 {{$board ? 'md-25' : 'md-33'}}">
    <article class="list-item-person">
        @if ($image)
        @if ($board == true || $senior != true)
        <div class="list-item-person__image">
            @else
            <a href="{{ get_permalink($ID)}}" class="list-item-person__image">
                @endif
                <div class="img-cover">
                    {!! $image !!}
                </div>
                @if ($board == true || $senior != true)
        </div>
        @else
        </a>
        @endif
        @endif
        <div class="list-item-person__content">
            <h1 class="list-item-person__title">
                @if ($board == true || $senior != true)
                {{ get_the_title($ID) }}
                @else
                <a href="{{ get_permalink($ID)}}">{{ get_the_title($ID) }}</a>
                @endif
            </h1>
            <p class="list-item-person__job-title ">{{get_post_meta($ID, 'job_title', 'true')}}</p>
        </div>
    </article>
</div>