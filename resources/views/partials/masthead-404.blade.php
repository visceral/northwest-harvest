@php
/**
* Masthead
* 404 masthead template
*
* To override on specifc pages or posts, use
* separate partials ('masthead-single') and
* include on their relative blade partial
*
*/
@endphp

<div class="masthead masthead--has-img" data-image-src="@asset('images/error404bg.jpg')">
    <span class="masthead__overlay img-bg" style="background-image: url(@asset('images/error404bg.jpg'));"></span>
    <div class="container">
        <div class="row justify-center">
            <div class="column xs-100 lg-67">
                <div class="masthead__content reveal reveal-up">
                    @if (function_exists('bcn_display'))
                    <p class="masthead__breadcrumbs breadcrumbs reveal reveal-up">{{ bcn_display() }}</p>
                    @endif
                    <h1 class="page-title reveal reveal-up">{!! App::title() !!}</h1>
                    <p class="masthead__sub reveal reveal-up">
                        {{ __('Sorry, but the page you were trying to view cannot be found.', 'visceral') }}</p>
                    <p class="text-center reveal reveal-up"><a href="/"
                            class="btn btn--white">{{ __('Back to homepage', 'visceral') }}</a></p>
                </div>
            </div>
        </div>
    </div>
</div>