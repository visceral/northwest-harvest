<header class="header">
    <div class="header__inner container">
        <a class="header__brand" href="{{ home_url('/') }}">
            <img src="@asset('images/nwh-logo-updated.svg')" alt="Northwest Harvest">
        </a>
        <div class="header__navigation">
            <nav class="header__secondary-navigation">
                @if (has_nav_menu('secondary_menu'))
                {!! App\bem_menu('secondary_menu', 'secondary-menu') !!}
                @endif
                <a href="/our-work/food-access-network"
                    class="hrn-link {{App::emergencyBanner() ? 'hrn-link--emergency': ''}}">{{ __('Find Food',
                    'visceral') }}
                    <i class="hrn-icon" aria-hidden="true"></i></a>
            </nav>
            <nav class="header__primary-navigation">
                @if (has_nav_menu('primary_menu'))
                {!! App\bem_menu('primary_menu') !!}
                @endif
            </nav>
            <div class="header__search">
                <div>
                    <form role="search" class="header__search-form searchform" method="get" action="{{ site_url() }}">
                        <label for="s"><span class="screen-reader-text">
                                <?php _e('Search', 'visceral'); ?>
                            </span>
                            <input type="text" placeholder="<?php _e('Begin typing keywords', 'visceral'); ?>..."
                                name="s" id="s" autocomplete="off" spellcheck="false" autofocus>
                        </label>
                        <input type="submit" class="screen-reader-text" value="<?php _e('Submit', 'visceral'); ?>">
                    </form>
                </div>
            </div>
            <a class="header__search-button" href="{{ site_url() }}/?s=" title="@php( _e('Search', 'visceral') )"
                aria-label="Search"><i class="icon-search"></i></a>
            <a href="https://northwestharvest.donorsupport.co/-/XCQEDMTS" class="header__donate btn btn--blue">{{
                __('Donate Now', 'visceral')}}</a>
        </div>
        <label class="header__nav-icon icon-nav no-print" for="nav-toggle" tabindex="0"><span
                class="screen-reader-text">Menu</span></label>
    </div>
</header>
@include('partials.mobile-menu')