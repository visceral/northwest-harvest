<div class="column xs-100">
    <div class="list-item-event">
        <div class="row">
            <div class="column xs-100 lg-25">
                <div class="list-item-event__entry-meta entry-meta">
                    <p class="post-label">{{App::taxonomyList($ID, 'event_type')}}</p>
                    <time class="updated">
                        {{ $post_fields['start_date'] }}
                        @if ($post_fields['end_date'])
                        - {{ $post_fields['end_date'] }}
                        @endif
                    </time>
                    <p class="list-item-event__county small">{{App::taxonomyList($ID, 'event_county')}}</p>
                </div>
            </div>
            <div class="column xs-100 lg-75">
                <div class="list-item-event__content">
                    <h2 class="list-item-event__title"><a href="{{ get_permalink($ID)}}">{{ get_the_title($ID) }}</a>
                    </h2>
                    <p class="list-item-event__excerpt">{{ App\truncate_text(get_the_excerpt($ID), 150, '...') }}</p>
                </div>
            </div>
        </div>
    </div>
</div>