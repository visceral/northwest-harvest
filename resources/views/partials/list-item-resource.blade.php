<div class="column xs-100">
    <article class="list-item-resource">
        <div class="row">
            @if ($image)
            <div class="column xs-100 lg-33">
                <div class="list-item-resource__image">
                    {!!$image!!}
                </div>
            </div>
            @endif
            <div class="column xs-100 lg-auto">
                <div class="list-item-resource__content">
                    <p class="post-label">{{App::taxonomyList($ID, 'resource_topic')}}</p>
                    <h1 class="list-item-resource__title h4">{{ get_the_title($ID) }}</h1>
                    <div class="list-item-resource__entry-meta entry-meta">
                        <time class="updated"
                            datetime="{{ get_post_time('c', true) }}">{{ get_the_date('F d, Y', $ID) }}</time>
                    </div>
                    {!! $download_link !!}
                </div>
            </div>
        </div>
    </article>
</div>