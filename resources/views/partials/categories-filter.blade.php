<div class="container">
    <section class="posts-filter">
        <div class="row justify-center">
            <div class="column xs-100 lg-80">
                <ul>
                    <li class="cat-item cat-item-all"><a href="/news-insights/">All</a></li>
                    {{ wp_list_categories(array('title_li' => ''))}}
                </ul>
            </div>
        </div>        
    </section>
</div>