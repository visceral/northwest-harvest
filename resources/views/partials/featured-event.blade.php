@php
    $bg_img = wp_get_attachment_image_src(get_post_thumbnail_id($featured_event[0]->ID), 'full');
@endphp
<section class="featured-post reveal reveal-up {{has_post_thumbnail($featured_event[0]->ID) ? 'featured-post--has-img' : ''}}">
    <div class="container">
        <div class="featured-post__inner" style="background-image: url({{has_post_thumbnail($featured_event[0]->ID) ? $bg_img[0] : ''}});">
            <div class="row">
                @if (has_post_thumbnail($featured_event[0]->ID))
                    <div class="column xs-100 lg-50">
                        <a href="{{ get_permalink($featured_event[0]->ID)}}" class="featured-post__image">
                            <div class="img-cover">
                                {!! App\get_aspect_ratio_image($featured_event[0]->ID)!!}
                            </div>
                        </a>
                    </div>
                    
                @endif
                <div class="column xs-100 lg-auto">
                    <div class="featured-post__content">
                        <p class="featured-post__post-label post-label">Featured Event</p>
                        <h2 class="featured-post__title"><a href="{{ get_permalink($featured_event[0]->ID)}}">{{ get_the_title($featured_event[0]->ID) }}</a></h2>
                        <p class="featured-post__excerpt">{{ App\truncate_text(get_the_excerpt($featured_event[0]->ID), 150, '') }}</p>
                            
                        <div class="featured-post__entry-meta entry-meta">
                            @if (App::postAuthors($featured_event[0]->ID))
                                <p>{!!App::postAuthors($featured_event[0]->ID)!!}</p>
                            @endif
                            <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date('F j, Y', $featured_event[0]->ID) }}</time>   
                        </div> 
                    </div>
                </div>
            </div>
        </div>              
    </div>
</section> 