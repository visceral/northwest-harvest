<input type="checkbox" id="nav-toggle">

<div class="mobile-nav-container">
    <div class="container-fluid">
        @if (has_nav_menu('mobile_menu'))
        {!! App\bem_menu('mobile_menu') !!}
        @endif
        <a href="https://northwestharvest.donorsupport.co/-/XCQEDMTS" class="btn donate">Donate</a>
        @if (has_nav_menu('secondary_menu'))
        {!! App\bem_menu('secondary_menu', 'secondary-menu') !!}
        @endif
        <a href="/our-work/food-access-network"
            class="hrn-link {{App::emergencyBanner() ? 'hrn-link--emergency': ''}}">{{ __('Find Partner Food Programs',
            'visceral') }}
            <i class="hrn-icon" aria-hidden="true"></i></a>
        <form role="search" class="mobile-nav__search-form searchform" method="get" action="{{ site_url() }}">
            <label for="mobile-s"><span>
                    <?php _e('Search', 'visceral'); ?>
                </span></label>
            <input type="text" placeholder="<?php _e('Type here and hit enter', 'visceral'); ?>..." name="s"
                id="mobile-s" autocomplete="off" spellcheck="false" autofocus>

            <button type="submit"><span class="screen-reader-text">
                    <?php _e('Submit', 'visceral'); ?>
                </span><i class="icon-search"></i></button>
        </form>
    </div>

</div>