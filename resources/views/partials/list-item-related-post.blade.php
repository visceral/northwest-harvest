<div class="column xs-100 md-33">
    <div class="list-item-related-post reveal">
        <div class="list-item-related-post__content">
            <p class="post-label">{{App::postLabels()}}</p>
            <h2 class="list-item-related-post__title">
                @if (get_field('external_link', $post_id))
                <a href="{{ get_field('external_link', $post_id) }}">{{ get_the_title($post_id) }}<span
                        class='icon-offsite' aria-hidden='true'></span></a>
                @elseif (get_field('publication_url', $post_id))
                <a href="{{ get_field('publication_url', $post_id) }}">{{ get_the_title($post_id) }}<span
                        class='icon-download' aria-hidden='true'></span></a>
                @else
                <a href="{{ get_permalink() }}">{{ get_the_title($post_id) }}</a>
                @endif
            </h2>
            <p class="list-item-post__topic small">{{App::postTopics()}}</p>
        </div>
    </div>
</div>