<div class="container">
    <div class="row justify-center">
        <div class="column xs-100 lg-75">
            <article @php(post_class())>
                <section class="single-person__meta">
                    @if ($post_fields['linkedin'] || ($post_fields['twitter']))
                    <div class="single-person__meta-social">
                        @if ($post_fields['twitter'])
                        <a href="{{$post_fields['twitter']}}" aria-hidden="true">
                            <i class="icon-twitter" aria-hidden="true"></i><span
                                class="screen-reader-text">Twitter</span>
                        </a>
                        @endif

                        @if ($post_fields['linkedin'])
                        <a href="{{$post_fields['linkedin']}}">
                            <i class="icon-linkedin" aria-hidden="true"></i><span
                                class="screen-reader-text">LinkedIn</span>
                        </a>
                        @endif

                        Follow
                        {{$post_fields['first_name']}}
                    </div>
                    @endif
                    @if ($post_fields['email'])
                    <div class="single-person__meta-email">
                        <a href="mailto:{{$post_fields['email']}}"><i class="icon-email" aria-hidden="true"></i>Email
                            {{$post_fields['first_name']}}
                        </a>
                    </div>
                    @endif
                    @if ($post_fields['phone_number'])
                    <div class="single-person__meta-phone">
                        <a href="tel:{{$post_fields['phone_number']}}"><i class="icon-phone" aria-hidden="true"></i>
                            {{$post_fields['phone_number']}}
                        </a>
                    </div>
                    @endif
                </section>
                <div class="entry-content">
                    @php(the_content())
                </div>
            </article>
        </div>
    </div>
</div>
@if ($recent_posts)
<section class="recent-posts">
    <div class="container">
        <div class="recent-posts__heading h5">{{ __('Recent Posts from ', 'visceral')}} {{$post_fields['first_name']}}
            {{$post_fields['last_name']}}</div>
        <div class="row">
            @php
            global $post;
            setup_postdata($post);
            @endphp
            @foreach ($recent_posts as $post)
            @include('partials.list-item-related-post')
            @endforeach
            @php(wp_reset_postdata())
        </div>
    </div>
</section>
@endif

<?php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_theme_file_uri() . '/dist/images/NWH-logo.png'; // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Person",
			"worksFor" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
            ),
            "name" => get_the_title(),
            "url" => get_permalink(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
            ),
            'jobTitle' => $post_fields['job_title']
		);
		?>
<script type='application/ld+json'>
    <?php echo json_encode( $json_ld ); ?>
</script>
<?php 
endif; 
?>