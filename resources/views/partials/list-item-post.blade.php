<div class="column xs-100">
    <div class="list-item-post">
        <div class="row">
            <div class="column xs-100 lg-25">
                <div class="list-item-post__entry-meta entry-meta">
                    <p class="post-label">{{App::postLabels()}}</p>
                    @if (App::postAuthors())
                    <p class="list-item-post__author">{!!App::postAuthors()!!}</p>
                    @endif
                    <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
                </div>
            </div>
            <div class="column xs-100 lg-75">
                <div class="list-item-post__content">
                    <h2 class="list-item-post__title h4">
                        @if (get_field('external_link', $post_id))
                        <a href="{{ get_field('external_link', $post_id) }}"><span>{{ get_the_title($post_id) }}</span><span
                                class='icon-offsite' aria-hidden='true'></span></a>
                        @elseif (get_field('publication_url', $post_id))
                        <a href="{{ get_field('publication_url', $post_id) }}"><span>{{ get_the_title($post_id) }}</span><span
                                class='icon-download' aria-hidden='true'></span></a>
                        @else
                        <a href="{{ get_permalink() }}"><span>{{ get_the_title($post_id) }}</span></a>
                        @endif
                    </h2>
                    <p class="list-item-post__excerpt">{{ App\truncate_text(get_the_excerpt($post_id), 150, '...') }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>