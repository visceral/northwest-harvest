<article @php(post_class())>
    <header class="single__header">
        <div class="container">
            <div class="row justify-center">
                <div class="column xs-100 lg-67">
                    @if (App::postLabels())
                    <p class="post-label reveal reveal-up">{{App::postLabels()}}</p>
                    @endif
                    <h1 class="entry-title reveal reveal-up">{{ get_the_title() }}</h1>
                    <div class="entry-meta reveal reveal-up">
                        @if (App::postAuthors())
                        <p class="entry-meta__author">
                            {!!App::postAuthors()!!}
                        </p>
                        @endif

                        <time class="updated reveal reveal-up"
                            datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="entry-content">
        <div class="container">
            <div class="row justify-center">
                <div class="column xs-100 lg-67">
                    <div class="social-sharing-container">
                        <ul class="social-sharing">
                            <li class="social-sharing__item"><a target="_blank" rel="noreferrer" 
                                    href="https://twitter.com/intent/tweet?source={{ urlencode ($this_url) }}&amp;text={{ get_the_title()  . ': ' . urlencode (get_permalink()) }}"><i
                                        class="icon-twitter"></i><span
                                        class="screen-reader-text">Share on
                                        Twitter</span></a>
                            </li class="social-sharing__item">
                            <li class="social-sharing__item"><a target="_blank" rel="noreferrer" 
                                    href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode (get_permalink()) }}&amp;t={{ get_the_title() }}"><i
                                        class="icon-facebook"></i><span
                                        class="screen-reader-text">Share on
                                        Facebook</span></a>
                            </li class="social-sharing__item">
                            <li class="social-sharing__item"><a target="_blank" rel="noreferrer" 
                                    href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ urlencode (get_permalink())}}&amp;title={{ get_the_title() }}&amp;summary={{ urlencode ( get_the_excerpt() ) }}"><i
                                        class="icon-linkedin"></i><span
                                        class="screen-reader-text">Share on
                                        LinkedIn</span></a>
                            </li>
                            <li class="social-sharing__item"><a target="_blank" rel="noreferrer" href="mailto:?subject={{get_the_title()}}&body={{get_the_title() . ': ' . urlencode(get_permalink())}}"
                                    ><i class="icon-email"></i><span
                                        class="screen-reader-text">Share on
                                        Email</span></a>
                            </li>
                        </ul>
                    </div>
                    @if (has_post_thumbnail())
                    <div class="single__featured-image">
                        {!! get_the_post_thumbnail(get_the_id(), 'large') !!}
                    </div>
                    @endif
                    @php(the_content())
                </div>
            </div>
        </div>
    </div>
</article>
@if (count($related_posts) > 1)
<section class="related-posts">
    <div class="container">
        <h2 class="related-posts__heading h5 reveal">View Related News & Insights</h2>
        <div class="row">
            @foreach ($related_posts as $post)
            @php(setup_postdata($GLOBALS['post'] = $post))
            @include('partials.list-item-related-post')
            @endforeach
            @php(wp_reset_postdata())
        </div>
    </div>
</section>
@endif

@php
	/**
	 * Adding JSON-LD for structured data and SEO
	 *
	 * Official website: 	http://json-ld.org/
	 * Examples: 			http://jsonld.com/
	 * Generator: 			https://www.schemaapp.com/tools/jsonld-schema-generator/
	 * Testing: 			https://search.google.com/structured-data/testing-tool/u/0/
	 * Google Docs: 		https://developers.google.com/search/docs/data-types/data-type-selector
	 **/

	$logo_url = get_theme_file_uri() . '/dist/images/NWH-logo.png'; // Feel free to customize. Otherwise will use WordPress site icon

	if ( $logo_url) :
		// Images are required. We only continue if we have them.
		$featured_img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', true );
		
		// JSON-LD markup for Articles
		$json_ld = array(

			// Required
			"@context" => "http://schema.org",
			"@type" => "Article",
			"publisher" => array(	 
				"@type" => "Organization",
				"url" => get_site_url(),
				"name" => get_bloginfo('name'),
				"logo" => array(
							"@type" => "ImageObject",
				            "name" => get_bloginfo('name') . " Logo",
				            "width" => "512",
				            "height" => "512",
				            "url" => $logo_url
				),
            ),
            "headline" => get_the_title(),
            // TODO: This needs to be the custom field
			"author" => array(
				"@type" => "Person",
				"name" => get_the_author()
			),
			"datePublished" => get_the_date(),
			"image" => array(
				"@type" => "ImageObject",
				"url" => $featured_img[0],
				"width" => $featured_img[1],
				"height" => $featured_img[2]
			),

			// Recommended
			"dateModified" =>  get_the_modified_date(),
			"mainEntityOfPage" => get_permalink(),

			// Optional (Not sure if they affect SEO)
			// "url" => get_permalink(),
			// "description" => get_the_excerpt(),
			// "articleBody" => get_the_content(),
        );
    endif;    
@endphp
<script type='application/ld+json'>

    @php
        echo json_encode( $json_ld );
    @endphp
</script>
