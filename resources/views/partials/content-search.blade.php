<?php
$column         = has_post_thumbnail() ? 'sm-67' : 'sm-100';
$featured_image = App\get_aspect_ratio_image(4, 3, 'medium');
$excerpt        = wp_trim_words(get_the_excerpt(), 50 );
$type           = get_post_type();
$link = get_permalink();
if( $type == 'post') {
  
  $categories = get_the_terms(get_the_id(), 'category');

  if (!empty($categories)) {
    $type = $categories[0]->name;
  }
}
?>

<article @php(post_class( 'column xs-100 list-item-result' ))>
    <div class="row">
        <div class="column {!! $column !!}">
            <header>
            <span class="post-label list-item-result__post-label">{!! $type !!}</span>
            <h3 class="list-item-result__title entry-title"><a href="{{ $link }}">{!! get_the_title() !!}</a></h3>
            </header>
            <div class="entry-summary">
            <p>{!! $excerpt !!}</p>
            </div>
        </div>
        <?php
        if (has_post_thumbnail()) {
            echo '<div class="column sm-33 text-center">' . $featured_image . '</div>';
        }
        ?>
    </div>    
</article>