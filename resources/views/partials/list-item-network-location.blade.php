<div class="column xs-100 md-50 lg-25">
    <article class="list-item-network-location list-item-network-location--{{$program}}">
        <div class="list-item-network-location__content">
            <h1 class="list-item-network-location__title">
                @if (get_post_meta($ID, 'location_url', true))
                    <a href="{{get_post_meta($ID, 'location_url', true)}}">{{ get_the_title($ID) }}</a>
                @else
                {{ get_the_title($ID) }}
                @endif
               
            </h1>
            @if (get_post_meta($ID, 'address', true))
                <p class="list-item-network-location__address">{!! get_post_meta($ID, 'address', true)!!}</p>
            @endif
            <span class="list-item-network-location__divider"></span>
            @if ($fields['phone_number'])
            <p class="list-item-network-location__phone">{{$fields['phone_number']}}</p>
            @endif
            @if ($fields['hours'])
            <p class="list-item-network-location__label">Hours:</p>
            <p class="list-item-network-location__hours">{{$fields['hours']}}</p>
            @endif
        </div>
    </article>
</div>