<article @php(post_class())>
    <header class="single__header">
        <div class="container">
            <div class="row justify-center">
                <div class="column xs-100 lg-75">
                    <h1 class="entry-title">{{ get_the_title() }}</h1>
                    <div class="entry-meta">
                        <p class="post-label">{{App::taxonomyList(get_the_id(), 'event_type')}}</p>
                        <time>
                            {{ $post_fields['start_date'] }}
                            @if ($post_fields['end_date'])
                            - {{ $post_fields['end_date'] }}
                            @endif
                        </time>
                        <p class="entry-meta__county small">{{App::taxonomyList(get_the_id(), 'event_county')}}</p>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <div class="entry-content">
        <div class="container">
            <div class="row justify-center">
                <div class="column xs-100 lg-75">
                    <div class="row">
                        <div class="column xs-100 lg-25">
                            <div class="single-event-details">
                                @if ($post_fields['location'])
                                <div class="single-event-details__section">
                                    <p class="single-event-details__label">Location</p>
                                    <div class="small">{!! $post_fields['location'] !!}</div>
                                </div>
                                @endif
                                @if ($post_fields['start_time'])
                                <div class="single-event-details__section">
                                    <p class="single-event-details__label">Time</p>
                                    <div class="small">
                                        <p>{{ $post_fields['start_time'] }}
                                            @if ($post_fields['end_time'])
                                            - {{ $post_fields['end_time'] }}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                @endif
                                @if ($post_fields['link_url'])
                                <div class="single-event-details__section">
                                    <a href="{{$post_fields['link_url']}}"
                                        class="btn">{{$post_fields['link_text'] ? $post_fields['link_text'] : 'Register'}}</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="column xs-100 lg-auto">
                            @php(the_content())
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</article>


