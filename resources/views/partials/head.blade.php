<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script>
        // Prevents flash of unstyle cotnent for JS manipulated elements.
		document.querySelector('html').classList.remove('no-js');
    </script>
    <link rel="stylesheet" href="https://use.typekit.net/imi6qdp.css">
    <!-- Fundraise Up: world-class checkout experience for serious online fundraising -->
    <script>
        (function(w,d,s,n,a){if(!w[n]){var l='call,catch,on,once,set,then,track'
    .split(','),i,o=function(n){return'function'==typeof n?o.l.push([arguments])&&o
    :function(){return o.l.push([n,arguments])&&o}},t=d.getElementsByTagName(s)[0],
    j=d.createElement(s);j.async=!0;j.src='https://cdn.fundraiseup.com/widget/'+a;
    t.parentNode.insertBefore(j,t);o.s=Date.now();o.v=4;o.h=w.location.href;o.l=[];
    for(i=0;i<7;i++)o[l[i]]=o(l[i]);w[n]=o}
    })(window,document,'script','FundraiseUp','AEVYBPZT');
    </script>
    <!-- End Fundraise Up -->
    @php(wp_head())
</head>