@extends('layouts.app')

@section('content')
@while(have_posts()) @php
the_post()
@endphp
@section('masthead')
@include('partials.masthead')
@endsection
<div class="container">
    @include('partials.content-page')
</div>
<div class="container">
    <div class="public-drop-sites-filter">
        <form action="" class="public-drop-sites-filter__form">
            <label class="screen-reader-text" for="public_drop_site_county">County</label>
            <select name="public_drop_site_county" id="public_drop_site_county">
                <option value="">Select a County</option>
                @if ($public_drop_site_counties)
                @foreach ($public_drop_site_counties as $county)
                @php($selected = (isset($_GET['public_drop_site_county']) && sanitize_text_field(
                $_GET['public_drop_site_county'] ) == $county->slug ) ? 'selected' : '')
                <option value="{{$county->slug}}" {{$selected}}>{{$county->name}}</option>
                @endforeach
                @endif
            </select>
            <label class="screen-reader-text" for="public_drop_site_city">City</label>
            <select name="public_drop_site_city" id="public_drop_site_city">
                <option value="">Select a City</option>
                @if ($public_drop_site_cities)
                @foreach ($public_drop_site_cities as $city)
                @php($selected = (isset($_GET['public_drop_site_city']) && sanitize_text_field(
                $_GET['public_drop_site_city'] ) == $city->slug ) ? 'selected' : '')
                <option value="{{$city->slug}}" {{$selected}}>{{$city->name}}</option>
                @endforeach
                @endif
            </select>
            <label class="screen-reader-text" for="public_drop_site_search">Search</label>
            <input name="public_drop_site_search" id="public_drop_site_search" type="text" placeholder="Search...">
            <button class="btn btn--white" type="submit">Submit</button>
        </form>
    </div>
</div>
<div class="container">
    <section class="posts-list public-drop-sites-list">
        <div class="row justify-center">
            <div class="column xs-100 lg-83">
                @if ($drop_sites)
                <table class="drop-sites-table">
                    <tbody>
                        <tr>
                            <th>City</th>
                            <th>Zip</th>
                            <th>Phone</th>
                            <th>Location & Address</th>
                        </tr>
                        @foreach ($drop_sites as $drop_site)
                        <tr>
                            <td>{{ $drop_site['city']}}</td>
                            <td>{{ $drop_site['zip']}}</td>
                            <td width="200px">{{ $drop_site['fields']['phone']}}</td>
                            <td>
                                @if ($drop_site['fields']['location_url'])
                                <a
                                    href="{{$drop_site['fields']['location_url']}}">{{ get_the_title($drop_site['ID']) }}</a>
                                @else
                                {{ get_the_title($drop_site['ID']) }}
                                @endif
                                {{ $drop_site['fields']['address']}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="drop-sites-mobile">
                    @foreach ($drop_sites as $drop_site)
                    <table>
                        <tbody>
                            <tr>
                                <th>City</th>
                                <td style="width: 75%;">{{ $drop_site['city']}}</td>
                            </tr>
                            <tr>
                                <th>Zip</th>
                                <td style="width: 75%;">{{ $drop_site['zip']}}</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td style="width: 75%;">{{ $drop_site['fields']['phone']}}</td>
                            </tr>
                            <tr>
                                <th>Location & Address</th>
                                <td style="width: 75%;">
                                    @if ($drop_site['fields']['location_url'])
                                    <a
                                        href="{{$drop_site['fields']['location_url']}}">{{ get_the_title($drop_site['ID']) }}</a>
                                    @else
                                    {{ get_the_title($drop_site['ID']) }}
                                    @endif
                                    {{ $drop_site['fields']['address']}}</td>
                            </tr>
                        </tbody>
                    </table>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </section>
    <div class="back-to-top">
        <a href="#main-content">
            <svg width="43px" height="43px" viewBox="0 0 43 43" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Style-Guide" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Buttons-&amp;-Components" transform="translate(-1107.000000, -190.000000)">
                        <g id="Group-7" transform="translate(1107.000000, 190.000000)">
                            <circle id="Oval-Copy-5" fill="#00334D"
                                transform="translate(21.500000, 21.500000) rotate(-180.000000) translate(-21.500000, -21.500000) "
                                cx="21.5" cy="21.5" r="21.5"></circle>
                            <path
                                d="M15.6187762,17.1134648 C16.0757548,16.6592242 16.7956632,16.6310129 17.2853141,17.0273494 L17.3865352,17.1187762 L21.475,21.231 L25.6187603,17.1134807 C26.0757307,16.6592318 26.7956386,16.6310076 27.2852966,17.0273353 L27.3865194,17.1187603 C27.8407683,17.5757307 27.8689924,18.2956387 27.4726648,18.7852967 L27.3812397,18.8865194 L21.4647485,24.7677749 L15.6134648,18.8812238 C15.1267784,18.391604 15.1291563,17.6001513 15.6187762,17.1134648 Z"
                                id="Combined-Shape-Copy" fill="#FFFFFF" fill-rule="nonzero"
                                transform="translate(21.500000, 20.758887) scale(-1, -1) translate(-21.500000, -20.758887) ">
                            </path>
                        </g>
                    </g>
                </g>
            </svg>
        </a>
    </div>
    @endwhile
    @endsection