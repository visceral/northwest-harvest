@extends('layouts.app')

@section('content')
<div class="container">
    @while(have_posts()) @php(the_post())
    @section('masthead')
    @include('partials.masthead')
    @endsection
    @include('partials.content-page')

    <div class="row justify-center">
        <div class="column xs-100">
            <section>
                <div class="row">
                    @foreach ($people->posts as $person)
                    @include('partials/list-item-person', $person)
                    @endforeach
                </div>
            </section>
        </div>
    </div>
    @endwhile
</div>
@endsection