@extends('layouts.app')

@section('content')
@while(have_posts()) @php(the_post())
@section('masthead')
@include('partials.masthead')
@endsection
<div class="container">
<section>
    <h1>Elements</h1>
    <hr>
    <h1>Heading 1</h1>
    <h2>Heading 2</h2>
    <h3>Heading 3</h3>
    <h4>Heading 4</h4>
    <h5>Heading 5</h5>
    <h6>Heading 6</h6>
    <h2 class="buffer">Heading with Buffer</h2>
    <p>Paragraph Lorem ipsum dolor, sit amet <a href="#">Link consectetur</a> adipisicing elit. Eum, eos neque,
        exercitationem rem incidunt quis delectus, adipisci quisquam laboriosam nulla officia autem architecto molestias
        vitae vero nam! Harum, quidem dignissimos?</p>
    <a href="#" class="link--underline">Underline Link</a>
    <h3> <a href="#" class="link--underline">Heading Underline Link</a></h3>


    <ul>
        <li>adipisci quisquam laboriosam nulla officia</li>
        <li>arum, quidem dignissimos</li>
        <li>laboriosam nulla</li>
        <li>sit amet <a href="#">Link consectetur</a> adipisicing elit</li>
    </ul>
    <ol>
        <li>adipisci quisquam laboriosam nulla officia</li>
        <li>arum, quidem dignissimos</li>
        <li>laboriosam nulla</li>
        <li>sit amet <a href="#">Link consectetur</a> adipisicing elit</li>
    </ol>
    <blockquote>Blockquote Lorem ipsum dolor sit amet consectetur adipisicing elit.</blockquote>
    <blockquote>
        <p>
            Blockquote dolor sit amet consectetur adipisicing elit.
        </p>
        <cite>Citation Name</cite>
    </blockquote>
    <table>
        <tbody>
            <tr>
                <th>Table heading</th>
                <th>Table heading</th>
                <th>Table heading</th>
                <th>Table heading</th>
            </tr>
            <tr>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
            </tr>
            <tr>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
            </tr>
            <tr>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
            </tr>
            <tr>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
                <td>Table data</td>
            </tr>
        </tbody>
    </table>
    <h2 class="buffer">Horizontal Rules</h2>
    <hr>
    <hr class="hr--short">
</section>
<section>
    <h1>Components</h1>
    <hr>
    <button>Button</button>
    <a href="#" class="btn">Link Button</a>
    <div style="background: gray; padding: 40px; margin: 40px 0;">
        <a href="#" class="btn btn--blue">Blue Button</a> <a href="#" class="btn btn--white">White Button</a>
    </div>
    <span class="post-label">Post Label</span>
    <form action="">
        <label for="text">Text Label</label>
        <input type="text">
        <label for="textarea">Textarea Label</label>
        <textarea name="textarea" id="textarea" cols="30" rows="10"></textarea>
        <label for="select">Select Label</label>
        <select name="select" id="select">
            <option value="1">Option 1</option>
            <option value="2">Option 2</option>
            <option value="3">Option 3</option>
        </select>
        <label for="checkbox">Checkbox Label</label>
        <input id="checkbox" type="checkbox">
        <label for="radio">Radio Label</label>
        <input id="radio" type="radio">
    </form>
</section>
<section>
    {{ the_content()}}
</section>
</div>
@endwhile
@endsection