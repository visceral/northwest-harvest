@extends('layouts.app')

@section('content')
@while(have_posts()) @php(the_post())
@section('masthead')
@include('partials.masthead')
@endsection
<div class="container">
    @if (App::emergencyBanner())
    <div class="emergency-banner">
        <i class="icon-alert" aria-hidden="true"></i>
        <p class="h6">Emergency Assistance</p>
        {!! App::emergencyBanner() !!}
    </div>
    @endif
    <div class="hrn-filter no-print">
        <form action="" class="hrn-filter__form">
            <label class="screen-reader-text" for="network_program">Program</label>
            <select name="network_program" id="network_program">
                <option value="">Select a Program</option>
                @if ($network_programs)
                @foreach ($network_programs as $program)
                @php($selected = $_REQUEST['network_program'] === $program->slug ? 'selected' : '')
                <option value="{{$program->slug}}" {{$selected}}>{{$program->name}}</option>
                @endforeach
                @endif
            </select>
            <label class="screen-reader-text" for="network_location_county">County</label>
            <select name="network_location_county" id="network_location_county">
                <option value="">Select a County</option>
                @if ($network_counties)
                @foreach ($network_counties as $county)
                @php($selected = $_REQUEST['network_location_county'] === $county->slug ? 'selected' : '')
                <option value="{{$county->slug}}" {{$selected}}>{{$county->name}}</option>
                @endforeach
                @endif
            </select>
            <button class="btn btn--white" type="submit">Submit</button>
        </form>
    </div>
</div>
<div class="container">
    <div id="hrn-map-container" class="no-js-hide no-print">
    </div>
    <section class="entry-content">
        @php(the_content())
    </section>
    <section class="posts-list hrn-posts-list {{count($locations) > 12 ? 'hrn-posts-list--more' : ''}}">
        <div class="row justify-center">
            <div class="column xs-100 lg-83">
                <div class="row">
                    @if (empty($locations))
                    <div class="column xs-100">No Locations match that criteria. Please try again.</div>
                    @else
                    @foreach ($locations as $location)
                    @include('partials/list-item-network-location', $location)
                    @endforeach
                    @endif

                </div>
                <button class="hrn-load-more no-js-hide" aria-label="Load More">Load More <i class="icon-chevron-down"
                        aria-hidden="true"></i></button>
            </div>

        </div>
    </section>
    <div class="back-to-top">
        <a href="#main-content">
            <svg width="43px" height="43px" viewBox="0 0 43 43" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
                <!-- Generator: Sketch 62 (91390) - https://sketch.com -->
                <title>Group 7</title>
                <desc>Created with Sketch.</desc>
                <g id="Style-Guide" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Buttons-&amp;-Components" transform="translate(-1107.000000, -190.000000)">
                        <g id="Group-7" transform="translate(1107.000000, 190.000000)">
                            <circle id="Oval-Copy-5" fill="#00334D"
                                transform="translate(21.500000, 21.500000) rotate(-180.000000) translate(-21.500000, -21.500000) "
                                cx="21.5" cy="21.5" r="21.5"></circle>
                            <path
                                d="M15.6187762,17.1134648 C16.0757548,16.6592242 16.7956632,16.6310129 17.2853141,17.0273494 L17.3865352,17.1187762 L21.475,21.231 L25.6187603,17.1134807 C26.0757307,16.6592318 26.7956386,16.6310076 27.2852966,17.0273353 L27.3865194,17.1187603 C27.8407683,17.5757307 27.8689924,18.2956387 27.4726648,18.7852967 L27.3812397,18.8865194 L21.4647485,24.7677749 L15.6134648,18.8812238 C15.1267784,18.391604 15.1291563,17.6001513 15.6187762,17.1134648 Z"
                                id="Combined-Shape-Copy" fill="#FFFFFF" fill-rule="nonzero"
                                transform="translate(21.500000, 20.758887) scale(-1, -1) translate(-21.500000, -20.758887) ">
                            </path>
                        </g>
                    </g>
                </g>
            </svg>
        </a>
    </div>
</div>
@endwhile
@endsection