<!doctype html>
<html @php(language_attributes()) class="no-js">
@include('partials.head')

<body @php(body_class())>
    <a id="skip-to-content" href="#main-content" class="no-print"><?php _e('Skip to content', 'visceral'); ?></a>
    @php(do_action('get_header'))
    @include('partials.header')
    @yield('masthead')
    <div class="wrap" role="document">
        <div class="content">
            <main class="main" id="main-content" tabindex="-1">
                @yield('content')
            </main>
            @if (App\display_sidebar())
            <aside class="sidebar">
                @include('partials.sidebar')
            </aside>
            @endif
        </div>
    </div>
    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())
</body>

</html>