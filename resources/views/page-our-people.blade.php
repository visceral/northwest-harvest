@extends('layouts.app')

@section('content')
<div class="container">
    @while(have_posts()) @php(the_post())
    @section('masthead')
    @include('partials.masthead')
    @endsection
    @include('partials.content-page')
    @endwhile
</div>
@endsection