@extends('layouts.app')

@section('content')
  @section('masthead')
    @include('partials.masthead')
  @endsection
@include('partials/categories-filter')
<div class="container">
    <div class="row justify-center">
        <div class="column xs-100 lg-75">
            @if (!have_posts())
                <div class="alert alert-warning">
                {{ __('Sorry, no results were found.', 'visceral') }}
                </div>
                {!! get_search_form(false) !!}
            @endif
            <div class="posts-list">
            @while (have_posts()) @php(the_post())
                @include('partials.list-item-'.get_post_type())
            @endwhile
            </div>
            {!! get_the_posts_navigation() !!}
        </div>
    </div>   
</div>
@endsection
