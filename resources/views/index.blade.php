@extends('layouts.app')

@section('content')
  @section('masthead')
    @include('partials.masthead')
  @endsection
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'visceral') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php(the_post())
    @include('partials.list-item-'.get_post_type())
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
