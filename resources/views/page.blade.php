@extends('layouts.app')
@section('masthead')
@include('partials.masthead')
@endsection
@section('content')
@while(have_posts()) @php(the_post())

@include('partials.content-page')
@endwhile
@endsection