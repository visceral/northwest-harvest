@extends('layouts.app')
@section('masthead')
@include('partials.masthead')
@endsection
@section('content')

{{-- @if ($featured_event)
@include('partials/featured-event')
@endif --}}

<div class="container">
    <div class="row justify-center">
        <div class="column xs-100 lg-75">
            <div>
                @while(have_posts()) @php(the_post())
                @include('partials.content-page')
                @endwhile
            </div>
            <div class="event-list">
                <div class="row">
                    @if ($events->posts)
                    @foreach ($events->posts as $event)
                    @include('partials/list-item-event', $event)
                    @endforeach
                    @else
                    <div class="column xs-100">
                        <p>{{ __('There are no upcoming events at this time. Check back later or ', 'visceral')}} <a
                                href="/stay-informed/">{{ __(' sign up for our newsletter', 'visceral')}}</a>{{ __(' to stay informed.', 'visceral')}}
                        </p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
@endsection