/* eslint-disable no-unused-vars */
// /* eslint-disable no-undef */
export default {
    init() {
        const google = window.google;
        // Initialize map variable
        var map;

        // Markers
        var markers = [];

        // Create bubble window for map markers
        var infowindow = new google.maps.InfoWindow();

        function initMap() {
            // Settings for map
            var args = {
                zoom: 7,
                center: new google.maps.LatLng(47.4281946, -120.3658671),
                disableDefaultUI: true,
                panControl: true,
                zoomControl: true,
                draggable: true,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };

            // Create the Map and attach it to the specific DOM element
            map = new google.maps.Map(
                document.getElementById('hrn-map-container'),
                args
            );

            // AJAX call to get location data
            filterNetwork();
        }

        initMap();

        function filterNetwork() {
            // Clear all existing markers
            markers.forEach(el => {
                el.setMap(null);
            });
            // Markers
            markers = [];

            // List Items
            var listItems = [];

            // Filters
            var program = $('select#network_program').val();
            var county = $('select#network_location_county').val();

            // Create bubble window for map markers
            var infowindow = new google.maps.InfoWindow();

            var url =
                '/wp-json/locations_endpoint/v1/locations?network_program=' +
                program +
                '&network_location_county=' +
                county;

            // AJAX call to get location data
            $.ajax({
                url: url,
                type: 'get',
                success: function(data) {
                    // Loop through each location
                    // If there are locations, add pins andchange zoom and center of map. Else, reset zoom and center to initial state
                    if (data.length > 0) {
                        data.forEach(el => {
                            // Create a new marker using the lat and lng data from the location
                            if (el['location']) {
                                var pinImage =
                                    '/wp-content/themes/northwest-harvest/dist/images/marker-pin-blue.png';

                                if (el['program'] === 'warehouse') {
                                    pinImage =
                                        '/wp-content/themes/northwest-harvest/dist/images/marker-pin-green.png';
                                }

                                if (el['program'] === 'meal-program') {
                                    pinImage =
                                        '/wp-content/themes/northwest-harvest/dist/images/marker-pin-orange.png';
                                }

                                var image = {
                                    url: pinImage,
                                    // This marker is 23 pixels wide by 30 pixels tall.
                                    size: new google.maps.Size(23, 30),
                                    // The origin for this image is 0,0.
                                    origin: new google.maps.Point(0, 0),
                                };
                                var latlng = new google.maps.LatLng(
                                    el['location']['lat'],
                                    el['location']['lng']
                                );
                                var marker = new google.maps.Marker({
                                    position: latlng,
                                    map: map,
                                    icon: image,
                                });

                                markers.push(marker);
                                var bounds = new google.maps.LatLngBounds();
                                markers.forEach(el => {
                                    bounds.extend(el.getPosition());
                                });

                                map.fitBounds(bounds, 20);

                                google.maps.event.addListener(
                                    marker,
                                    'click',
                                    function() {
                                        var markerContent = '';
                                        if (el['name']) {
                                            markerContent +=
                                                '<p class="iw-title">';

                                            if (el['location_url']) {
                                                markerContent +=
                                                    '<a href="' +
                                                    el['location_url'] +
                                                    '" target="_blank" rel="noopener">' +
                                                    el['name'] +
                                                    '</a>';
                                            } else {
                                                markerContent += el['name'];
                                            }

                                            markerContent += '</p>';
                                        }
                                        if (el['address']) {
                                            markerContent +=
                                                '<p>' + el['address'] + '</p>';
                                        }
                                        if (el['phone']) {
                                            markerContent +=
                                                '<p>' + el['phone'] + '</p>';
                                        }
                                        if (el['hours']) {
                                            markerContent +=
                                                '<p class="iw-label">Hours:</p><p class="iw-hours">' +
                                                el['hours'] +
                                                '</p>';
                                        }
                                        markerContent +=
                                            '<p><a href="http://maps.google.com/maps?q=' +
                                            el['address'] +
                                            '" target="_blank" rel="noopener">Get Directions</a></p>';
                                        infowindow.setContent(markerContent);
                                        infowindow.open(map, marker);
                                    }
                                );
                            }

                            var listItemContent = '';
                            listItemContent +=
                                '<div class="column xs-100 md-50 lg-25">';
                            listItemContent +=
                                '<article class="list-item-network-location list-item-network-location--' +
                                el['program'] +
                                '">';
                            listItemContent +=
                                '<div class="list-item-network-location__content">';
                            listItemContent +=
                                '<h1 class="list-item-network-location__title">';
                            if (el['location_url']) {
                                listItemContent += `<a href="${el['location_url']}" target="_blank" rel="noopener">${el['name']}</a>`;
                            } else {
                                listItemContent += el['name'];
                            }

                            listItemContent += '</h1>';
                            if (el['address']) {
                                '<p class="list-item-network-location__address">' +
                                    el['address'] +
                                    '</p>';
                            }
                            listItemContent +=
                                '<span class="list-item-network-location__divider"></span>';
                            if (el['phone_number']) {
                                listItemContent +=
                                    '<p class="list-item-network-location__phone">' +
                                    el['phone_number'] +
                                    '</p>';
                            }
                            if (el['hours']) {
                                listItemContent +=
                                    '<p class="list-item-network-location__label">Hours:</p>';
                                listItemContent +=
                                    '<p class="list-item-network-location__hours">' +
                                    el['hours'] +
                                    '</p>';
                            }
                            listItemContent += '</div>';
                            listItemContent += '</article>';
                            listItemContent += '</div>';

                            listItems.push(listItemContent);
                        });
                    } else {
                        map.setZoom(7);
                        var center = new google.maps.LatLng(
                            47.4281946,
                            -120.3658671
                        );
                        map.panTo(center);
                    }
                    var $postsContainer = $(
                        '.hrn-posts-list > .row > .column > .row'
                    );
                    $postsContainer.empty();

                    if (listItems.length === 0) {
                        $postsContainer.html(
                            '<div class="column xs-100">No Locations match that criteria. Please try again.</div>'
                        );
                    } else {
                        listItems.forEach(el => {
                            $postsContainer.append(el);
                        });
                    }

                    if (listItems.length > 12) {
                        $('.hrn-posts-list').addClass('hrn-posts-list--more');
                        $('.hrn-posts-list .hrn-load-more').show();
                    } else {
                        $('.hrn-posts-list').removeClass(
                            'hrn-posts-list--more'
                        );
                        $('.hrn-posts-list .hrn-load-more').hide();
                    }

                    // Browser History
                    var history_string;
                    if (program === '' && county === '') {
                        history_string = '?';
                    } else {
                        if (program !== '') {
                            history_string = '?network_program=' + program;

                            if (county !== '') {
                                history_string +=
                                    '&network_location_county=' + county;
                            }
                        } else if (county !== '') {
                            history_string =
                                '?network_location_county=' + county;
                        } else {
                            history_string = '';
                        }
                    }

                    history.pushState('', '', history_string);
                },
            });
        }

        $('.hrn-filter__form').on('submit', function(e) {
            e.preventDefault();
            filterNetwork();
        });

        $('.hrn-posts-list .hrn-load-more').on('click', function() {
            $(this).hide();
            $('.hrn-posts-list').removeClass('hrn-posts-list--more');
        });
    },
    finalize() {
        // JavaScript to be fired on the HRN page, after the init JS
    },
};
