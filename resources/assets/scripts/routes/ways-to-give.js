import DOMPurify from 'dompurify';

export default {
    init() {
        // JavaScript to be fired on the ways to give page
        var giftCalculator = $('#gift-calculation');
        var giftCalculatorOutput = document.querySelector(
            '.gift-calculator__output'
        );

        $('.gift-calculator__form').on('submit', function(e) {
            e.preventDefault();
        });

        giftCalculator.on('keyup', function() {
            setTimeout(() => {
                var amount = $(this).val();
                amount = DOMPurify.sanitize(parseFloat(amount), {
                    ALLOWED_TAGS: [],
                });
                var meals = Math.floor(amount / 0.22);
                meals = meals.toString();
                var regex = /^[a-zA-Z]+$/;
                if (amount === '') {
                    giftCalculatorOutput.innerHTML = '';
                } else if (!amount.match(regex)) {
                    giftCalculatorOutput.innerHTML =
                        'Your gift of $' +
                        amount.replace(/\B(?=(\d{3})+(?!\d))/g, ',') +
                        ' provides ' +
                        meals.replace(/\B(?=(\d{3})+(?!\d))/g, ',') +
                        ' meals.';
                } else {
                    giftCalculatorOutput.innerHTML = 'You must enter a number.';
                }
            }, 500);
        });
    },
};
