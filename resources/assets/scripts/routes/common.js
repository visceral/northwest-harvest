export default {
    init() {
        // JavaScript to be fired on all pages

        /** SKIP LINK NAVIGATION */
        $('#skip-to-content').click(function() {
            // strip the leading hash and declare
            // the content we're skipping to
            var skipTo = '#' + this.href.split('#')[1];
            // Setting 'tabindex' to -1 takes an element out of normal
            // tab flow but allows it to be focused via javascript
            $(skipTo)
                .attr('tabindex', -1)
                .on('blur focusout', function() {
                    // when focus leaves this element,
                    // remove the tabindex attribute
                    $(this).removeAttr('tabindex');
                })
                .focus(); // focus on the content container
        });
        /** END SKIP LINK NAVIGATION */

        /** MASTHEAD PRGRESSIVE IMAGES **/
        var masthead = document.querySelector('.masthead'),
            placeholderOverlay = document.querySelector('.masthead__overlay');

        if (masthead && placeholderOverlay) {
            // Load full size image. When loaded, fade our placeholder add it as bg to masthead
            var img = new Image();
            img.src = masthead.dataset.imageSrc;
            img.onload = function() {
                placeholderOverlay.classList.add('fade-out');
                masthead.style.backgroundImage = 'url(' + img.src + ')';
            };
        }
        /** END MASTHEAD PRGRESSIVE IMAGES **/

        /* VIDEO MODALS */
        $('.video-modal').magnificPopup({
            type: 'iframe',
            iframe: {
                markup:
                    '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                    '<div class="mfp-title">Some caption</div>' +
                    '</div>',
            },
            callbacks: {
                markupParse: function(template, values, item) {
                    values.title = item.el.attr('title');
                },
            },
        });
        /* END VIDEO MODALS */

        // Header Search
        let $headerSearchButton = $('.header__search-button');

        $headerSearchButton.on('click', function(e) {
            e.preventDefault();
            $('header.header').toggleClass('header--search-active');
        });

        /** ANIMATED ANCHOR LINKS **/
        $('a[href*="#"]:not([href="#"])').click(function(e) {
            // Doesn't do it on page builder tabs
            if (
                !$(this)
                    .closest('li')
                    .hasClass('vc_tta-tab') &&
                !$(this).is('.resources .taxonomy-filter__link')
            ) {
                if (
                    !$(this)
                        .closest('h4')
                        .hasClass('vc_tta-panel-title')
                ) {
                    if (
                        location.pathname.replace(/^\//, '') ===
                            this.pathname.replace(/^\//, '') &&
                        location.hostname === this.hostname
                    ) {
                        var target = $(this.hash);
                        var $this = this;
                        var header = $('header.header');
                        var wpAdminBar = $('#wpadminbar');
                        var fixedHeaderOffset =
                            parseInt(header.outerHeight()) -
                            parseInt(
                                header.css('padding-top').replace('px', '')
                            );
                        // If we're logged in and WP ADmin Bar exists, add it to the offset
                        if (wpAdminBar.length) {
                            fixedHeaderOffset += parseInt(
                                wpAdminBar.outerHeight()
                            );
                        }
                        target = target.length
                            ? target
                            : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate(
                                {
                                    scrollTop:
                                        target.offset().top -
                                        fixedHeaderOffset -
                                        30,
                                },
                                500,
                                function() {
                                    if (history.pushState) {
                                        history.pushState(
                                            null,
                                            null,
                                            $this.hash
                                        );
                                    } else {
                                        location.hash = $this.hash;
                                    }
                                }
                            );
                            e.preventDefault();
                        }
                    }
                }
            }
        });

        //Executed on page load with URL containing an anchor tag.
        if ($(location.href.split('#')[1])) {
            var target = $('#' + location.href.split('#')[1]);
            var header = $('header.header');
            var fixedHeaderOffset =
                parseInt(header.outerHeight()) -
                parseInt(header.css('padding-top').replace('px', ''));
            if (target.length) {
                $('html,body').animate(
                    {
                        scrollTop: target.offset().top - fixedHeaderOffset - 30, //offset height of header here too.
                    },
                    500
                );
            }
        }
        /** END ANIMATED ANCHOR LINKS **/

        // Animations
        function itemReveal() {
            var itemQueue = [];
            var delay = 100;
            var queueTimer;

            function processItemQueue() {
                
                if (queueTimer) return; // We're already processing the queue
                queueTimer = window.setInterval(function() {
                    if (itemQueue.length) {                        
                        $(itemQueue.shift()).addClass('revealed');
                        processItemQueue();
                    } else {
                        window.clearInterval(queueTimer);
                        queueTimer = null;
                    }
                }, delay);
            }

            $('.reveal').waypoint(
                function() {
                    itemQueue.push(this);
                    processItemQueue();
                },
                {
                    offset: '100%',
                }
            );

            $('.visc-stat').waypoint(
                function() {
                    itemQueue.push(this.element);
                    processItemQueue();
                },
                {
                    offset: '100%',
                }
            );
        }

        itemReveal();

        /** Break link text to new line when link text is a URL **/
        $(document).ready(function() {
            $('a').each(function() {
                var $this = $(this);
                var innerLink = $this.text();
                if (innerLink.startsWith('http')) {
                    $this.css('word-break', 'break-all');
                }
            });
        });

        /** End Break link text to new line when link text is a URL **/

        // Sticky Sidebar
        let $sideBarNav = $('.sidebar-nav');
        let $mainContent = $('#main-content');
        let sideBarNavOffset;
        let trigger;
        let mainContentOffset;
        let triggerRelease;

        // this is called outside the calculation function, because we only want the initial sidebar state for the trigger
        if ($sideBarNav.length > 0) {
            sideBarNavOffset = $sideBarNav.offset();
            trigger = sideBarNavOffset.top - 180;
        }
        calculateSidebarDistance();
        $(window).on('scroll', function() {
            stickySideNav();
        });
        $(window).on('resize', function() {
            stickySideNav();
        });

        $('.vc_toggle').on('click', function() {
            setTimeout(function() {
                calculateSidebarDistance();
            }, 1000);
        });

        function calculateSidebarDistance() {
            if ($mainContent.length > 0) {
                mainContentOffset = $mainContent.offset();
                triggerRelease =
                    mainContentOffset.top +
                    $mainContent.outerHeight() -
                    ($sideBarNav.outerHeight() + 360);
            }
        }
        function stickySideNav() {
            if (
                $(window).outerWidth() >= 992 &&
                $(window).outerHeight() >= 875
            ) {
                if ($sideBarNav.length > 0) {
                    let $scrollTop = $(window).scrollTop();

                    if ($scrollTop >= triggerRelease) {
                        $sideBarNav.removeClass('sidebar-nav--fixed');
                        $sideBarNav.addClass('sidebar-nav--absolute');
                    } else if ($scrollTop >= trigger) {
                        $sideBarNav.addClass('sidebar-nav--fixed');
                        $sideBarNav.removeClass('sidebar-nav--absolute');
                    } else {
                        $sideBarNav.removeClass('sidebar-nav--fixed');
                    }
                }
            } else {
                $sideBarNav
                    .removeClass('sidebar-nav--fixed')
                    .removeClass('sidebar-nav--absolute');
            }
        }

        // slick slide
        if ($('.visceral-slider').length > 0) {
            var slider = $(
                '.visceral-slider > .wpb_column > .vc_column-inner > .wpb_wrapper'
            );

            slider.slick({
                slide:
                    '.visceral-slider > .wpb_column > .vc_column-inner > .wpb_wrapper > *',
                autoplay: true,
                autoplaySpeed: 8000,
                infinite: true,
                centerMode: true,
                centerPadding: '0px',
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                dots: true,
                arrows: true,
                fade: false,
                mobileFirst: true,
                prevArrow:
                    '<button type="button" class="slick-prev"><span class="icon-chevron-right"></span><span class="screen-reader-text">Previous</span></button>',
                nextArrow:
                    '<button type="button" class="slick-next"><span class="icon-chevron-right"></span><span class="screen-reader-text">Next</span></button>',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            dots: false,
                        },
                    },
                ],
            });
        }

        // Blog Load More
        let prevLinkURL;
        let $prevLink = $('.navigation .nav-previous a');
        let $pagination = $('.navigation');
        if ($prevLink) {
            prevLinkURL = $prevLink.attr('href');
            $pagination
                .after(
                    '<button class="load-more" aria-label="Load More">Load More<i class="icon-chevron-down"></i></button>'
                )
                .hide();
        }

        function loadMore(url) {
            // $(containerSelector).load(url, '.posts-list')
            $.ajax({
                url: url,
                type: 'get',
                success: function(data) {
                    let $newPosts = $(data).find('.posts-list > div');
                    let $newPrevLink = $(data).find(
                        '.navigation .nav-previous a'
                    );

                    $newPosts.each(function() {
                        $('.posts-list').append($(this));
                    });

                    if ($newPrevLink.attr('href')) {
                        prevLinkURL = $newPrevLink.attr('href');
                    } else {
                        if ($('.load-more')) {
                            $('.load-more').hide();
                        }
                    }
                },
            });
        }

        $('.load-more').on('click', function() {
            loadMore(prevLinkURL);
        });

        let $mobileIcon = $('.header__nav-icon');

        $mobileIcon.on('click', function() {
            $(this)
                .toggleClass('icon-nav')
                .toggleClass('icon-close');
        });

        let $mobileMenuItemsWithChildren = $(
            '.mobile-nav-container .main-menu__item--menu-item-has-children:not(.main-menu__sub-menu__item)'
        );

        $mobileMenuItemsWithChildren.each(function() {
            $(this).append(
                '<i class="icon-chevron-down" aria-hidden="true"></i>'
            );
        });

        let $mobileMenuItemDownArrow = $(
            '.mobile-nav-container .main-menu__item--menu-item-has-children i'
        );

        $mobileMenuItemDownArrow.on('click', function() {
            $(this)
                .closest('li')
                .find('.main-menu__sub-menu--1')
                .slideToggle();
        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        // Sub Navigation positioning
        function subNavigationPosition() {
            var subMenu = $('.header .main-menu__sub-menu--1');
            var subMenuContainer = $('.header .main-menu__sub-menu .container');
            var headerInner = $('.header__inner');
            var headerNavigation = $('.header__primary-navigation');
            var headerNavOffset = headerNavigation.offset();

            var headerNavLeftPosition =
                headerNavOffset.left - $(window).scrollLeft();
            var subMenuOffset =
                ($(window).outerWidth() - headerInner.outerWidth()) / 2;
            subMenuContainer.css({
                'margin-left': headerNavLeftPosition,
                'max-width': headerNavigation.outerWidth() + 130,
            });
            subMenu.css({
                left: -subMenuOffset,
                right: -subMenuOffset,
            });
        }

        setTimeout(() => {
            subNavigationPosition();
        }, 500);

        $(window).resize(function() {
            subNavigationPosition();
        });
    },
};
