export default {
  init() {
    // JavaScript to be fired on the home page
    // slick slide
    if ($('.homepage-slides').length > 0) {
        var slider = $('.homepage-slides .row');

        slider.slick({
            slide: '.homepage-slides .column',
            autoplay: true,
            autoplaySpeed: 8000,
            infinite: true,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            swipeToSlide: true,
            dots: true,
            arrows: false,
            fade: false,
            mobileFirst: true,
        });
    }
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
