// import external dependencies
import 'jquery';
import 'slick-carousel';
import './vendor.js';
import 'magnific-popup';

// Import everything from autoload

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import waysToGive from './routes/ways-to-give';
import foodAccessNetwork from './routes/food-access-network';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to waysToGive.
  waysToGive,
  foodAccessNetwork,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
