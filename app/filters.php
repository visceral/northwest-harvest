<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    if (function_exists('get_field')) {
        if (get_field('emergency_alert_banner', 'option') && get_field('emergency_enable', 'option')) {
            $classes[] = 'emergency-alert';
        }

        if (is_home() || is_archive()) {
            if (get_field('featured_post', get_option('page_for_posts'))) {
                $classes[] = 'blog-featured-post';
            }
        }

        if (is_page('events')) {
            if (get_field('featured_event')) {
                $classes[] = 'events-featured-post';
            }
        }
    }


    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continue reading', 'sage') . '<span class="screen-reader-text"> ' . get_the_title() . '</span></a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory() . '/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Tell WordPress how to find the compiled path of comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );
    return template_path(locate_template(["views/{$comments_template}", $comments_template]) ?: $comments_template);
});


/**
 * Allow SVG Uploads
 */
function theme_allow_svg_uploads($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\theme_allow_svg_uploads');
// End Allow SVG Uploads

/**
 * Custom Font Formats
 */
function theme_tiny_mce_formats_button($buttons)
{
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', __NAMESPACE__ . '\\theme_tiny_mce_formats_button');

// Callback function to filter the MCE settings
function theme_tiny_mce_custom_font_formats($init_array)
{
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'New Section',
            'selector' => 'h1, h2, h3, h4, h5, h6',
            'classes' => 'buffer'
        ),
        array(
            'title' => 'Blue-Green Button',
            'selector' => 'a',
            'classes' => 'btn'
        ),
        array(
            'title' => 'Blue-White Button',
            'selector' => 'a',
            'classes' => 'btn--blue'
        ),
        array(
            'title' => 'White-Blue Button',
            'selector' => 'a',
            'classes' => 'btn--white'
        ),
        array(
            'title' => 'Underline Link',
            'selector' => 'a',
            'classes' => 'link--underline'
        ),
        array(
            'title' => 'Number Ticker',
            'inline' => 'span',
            'classes' => 'ticker'
        ),
        array(
            'title' => 'Small Green Horizontal Rule',
            'selector' => 'hr',
            'classes' => 'hr--short'
        )
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\theme_tiny_mce_custom_font_formats'); // Attach callback to 'tiny_mce_before_init'
// End Custom Font Formats

/**
 * Query for all post statuses so attachments are returned
 */
function visceral_modify_link_query_args($query)
{
    // Attachment post types have status of inherit. This allows them to be included.
    $query['post_status'] = array('publish', 'inherit');
    return $query;
}
add_filter('wp_link_query_args', __NAMESPACE__ . '\\visceral_modify_link_query_args');

/**
 * Link to media file URL instead of attachment page
 */
function visceral_modify_link_query_results($results, $query)
{
    foreach ($results as &$result) {
        if ('Media' === $result['info']) {
            $result['permalink'] = wp_get_attachment_url($result['ID']);
        }
    }
    return $results;
}

add_filter('wp_link_query', __NAMESPACE__ . '\\visceral_modify_link_query_results');

/**
 * Filter Search by Post Type
 */
function filter_search_by_post_type($post_types)
{
    if (!empty($_GET['filter'])) {
        $post_types = array(sanitize_text_field($_GET['filter']));
    }
    return $post_types;
}
add_filter('searchwp_indexed_post_types', __NAMESPACE__ . '\\filter_search_by_post_type');

/**
 * LOWER YOAST META BOX
 */
function yoast_dont_boast($html)
{
    return 'low';
}
add_filter('wpseo_metabox_prio', __NAMESPACE__ . '\\yoast_dont_boast');
// END LOWER YOAST META BOX



/**
 * WPBakery Page Builder overrides
 * Remove unneeded elements/options
 *
 * Hooks into 'init' action
 *
 */
if (function_exists("vc_remove_element")) {
    // Remove default elements
    vc_remove_element('vc_facebook');
    vc_remove_element('vc_tweetmeme');
    vc_remove_element('vc_googleplus');
    vc_remove_element('vc_pinterest');
    vc_remove_element('vc_widget_sidebar');
    vc_remove_element('vc_flickr');
    vc_remove_element('vc_basic_grid ');
    vc_remove_element('vc_media_grid ');
    vc_remove_element('vc_wp_search');
    vc_remove_element('vc_wp_meta');
    vc_remove_element('vc_wp_recentcomments');
    vc_remove_element('vc_wp_calendar');
    vc_remove_element('vc_wp_pages');
    vc_remove_element('vc_wp_tagcloud');
    vc_remove_element('vc_wp_text');
    vc_remove_element('vc_wp_categories');
    vc_remove_element('vc_wp_archives');
    vc_remove_element('vc_wp_rss');

    // Remove extraneous options from buttons element
    vc_remove_param("vc_btn", "shape");
    vc_remove_param("vc_btn", "style");
    //vc_remove_param("vc_btn", "color");
    vc_remove_param("vc_btn", "custom_background");
    vc_remove_param("vc_btn", "gradient_color_1");
    vc_remove_param("vc_btn", "gradient_color_2");
    vc_remove_param("vc_btn", "gradient_custom_color_1");
    vc_remove_param("vc_btn", "gradient_custom_color_2");
    vc_remove_param("vc_btn", "gradient_text_color");
    vc_remove_param("vc_btn", "gradient_color_2");
    vc_remove_param("vc_btn", "custom_text");
    vc_remove_param("vc_btn", "outline_custom_color");
    vc_remove_param("vc_btn", "outline_custom_hover_background");
    vc_remove_param("vc_btn", "outline_custom_hover_text");
}

function vc_update_defaults()
{
    if (function_exists('vc_update_shortcode_param')) {

        // Add custom colors to buttons
        $colors_arr = array(
            __('Teal (Primary)', 'js_composer') => 'teal',
        );
        $param = \WPBMap::getParam('vc_btn', 'color');
        $param['value'] = $colors_arr;
        vc_update_shortcode_param('vc_btn', $param);
    }
}

add_action('vc_after_init', __NAMESPACE__ . '\\vc_update_defaults');

function vc_remove_wp_admin_bar_button()
{
    remove_action('admin_bar_menu', array(vc_frontend_editor(), 'adminBarEditLink'), 1000);
}
add_action('vc_after_init', __NAMESPACE__ . '\\vc_remove_wp_admin_bar_button');

function visc_acf_init()
{
    // API Key is set up with client (nwhcomm@gmail.com)
    acf_update_setting('google_api_key', 'AIzaSyCba1H6Dg0BjwXQiGxnkHCrQfZbED7V9SY');
}

add_action('acf/init', __NAMESPACE__ . '\\visc_acf_init');

add_action('rest_api_init', __NAMESPACE__ . '\\rest_api_filter_add_filters');
/**
 * Add the necessary filter to each post type
 **/
function rest_api_filter_add_filters()
{
    foreach (get_post_types(array('show_in_rest' => true), 'objects') as $post_type) {
        add_filter('rest_' . $post_type->name . '_query', 'rest_api_filter_add_filter_param', 10, 2);
    }
}
/**
 * Add the filter parameter
 *
 * @param  array           $args    The query arguments.
 * @param  WP_REST_Request $request Full details about the request.
 * @return array $args.
 **/
function rest_api_filter_add_filter_param($args, $request)
{
    // Bail out if no filter parameter is set.
    if (empty($request['filter']) || !is_array($request['filter'])) {
        return $args;
    }
    $filter = $request['filter'];
    if (isset($filter['posts_per_page']) && ((int) $filter['posts_per_page'] >= 1 && (int) $filter['posts_per_page'] <= 100)) {
        $args['posts_per_page'] = $filter['posts_per_page'];
    }
    global $wp;
    $vars = apply_filters('rest_query_vars', $wp->public_query_vars);
    // Allow valid meta query vars.
    $vars = array_unique(array_merge($vars, array('meta_query', 'meta_key', 'meta_value', 'meta_compare')));
    foreach ($vars as $var) {
        if (isset($filter[$var])) {
            $args[$var] = $filter[$var];
        }
    }
    return $args;
}

function locations_main_endpoint($request_data)
{
    $program = $request_data->get_param('network_program');
    $county = $request_data->get_param('network_location_county');
    $args = array(
        'post_type'         => 'network_location',
        'posts_per_page'    => -1,
        'orderby'           => 'title',
        'order'             => 'ASC',
        'tax_query' => array(
            'relation' => 'AND',
        )
    );

    if ($program) {
        $args['tax_query'][] = array(
            'taxonomy' => 'network_program',
            'field'    => 'slug',
            'terms'    => $program,
        );
    }
    if ($county) {
        $args['tax_query'][] = array(
            'taxonomy' => 'network_location_county',
            'field'    => 'slug',
            'terms'    => $county,
        );
    }
    $posts = get_posts($args);

    // Build an array of all the data we need for the locations experience
    $locations = array();
    if (!empty($posts)) {
    foreach ($posts as $key => $post) {
        $locations[$key] = new \stdClass();
        $locations[$key]->id = $post->ID;
        $locations[$key]->name = get_the_title($post->ID);
        $locations[$key]->slug = $post->post_name;
        $locations[$key]->location_url = get_post_meta($post->ID, 'location_url', true);
        $locations[$key]->address = get_post_meta($post->ID, 'address', true);
        $locations[$key]->phone_number = get_post_meta($post->ID, 'phone_number', true);
        $locations[$key]->hours = get_post_meta($post->ID, 'hours', true);
        if (function_exists('get_field') && get_field('location', $post->ID)) {
            $locations[$key]->location = get_field('location', $post->ID);
        }
        $programs = get_the_terms($post->ID, 'network_program');

        if (!empty($programs && !is_wp_error($programs))) {
            $locations[$key]->program = $programs[0]->slug;
        }
    }
}

    $response = new \WP_REST_Response($locations);
    return $response;
}

add_action('rest_api_init', function () {
    register_rest_route('locations_endpoint/v1', '/locations/', array(
        'methods' => 'GET',
        'callback' => __NAMESPACE__ . '\\locations_main_endpoint',
    ));
}, 15);

/**
 * Redirect external news posts if accessed directly
 */
function external_post_redirect()
{
    if (is_singular('post')) {
        if (get_post_meta(get_the_ID(), 'external_link', true)) {
            wp_redirect(get_post_meta(get_the_ID(), 'external_link', true), 301);
        } elseif (get_post_meta(get_the_ID(), 'publication_url', true)) {
            wp_redirect(get_field('publication_url', get_the_ID()), 301);
        }
    }

    if (is_singular('resource')) {
        if (function_exists('get_field')) {
            if (get_field('external_url')) {
                wp_redirect(get_field('external_url', get_the_ID()), 301);
            } elseif (get_field('file')) {
                wp_redirect(get_field('file', get_the_ID()), 301);
            }
        }
    }
}
add_action('template_redirect', __NAMESPACE__ . '\\external_post_redirect');

/**
 * Redirect a non-senior person if accessed directly
 */
function non_senior_person_redirect()
{
    if (is_singular('person')) {
        $person_departments = get_the_terms(get_the_id(), 'person_department');
        $redirect = true;
        foreach ($person_departments as $person_department) {
            if ($person_department->slug === 'leadership-team') {
                $redirect = false;
                break;
            }
        }

        if ($redirect) {
            wp_redirect('/about-us/our-people/', 301);
        }
    }
}
add_action('template_redirect', __NAMESPACE__ . '\\non_senior_person_redirect');

// Changes archive title to not include taxonomy
add_filter('get_the_archive_title', function ($title) {
    if (is_category()) {
        $title = single_cat_title('', false);
    } elseif (is_tag()) {
        $title = single_tag_title('', false);
    } elseif (is_author()) {
        $title = get_the_author();
    } elseif (is_tax()) { //for custom post types
        $title = sprintf(__('%1$s'), single_term_title('', false));
    }
    return $title;
});
