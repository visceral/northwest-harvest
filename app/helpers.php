<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Emergency Alert',
        'menu_title' => 'Emergency Alert',
        'menu_slug' => 'emergency-alert',
        'icon_url' => 'dashicons-warning'
    ));
}

/**
 * Visceral Query
 *
 * @return  The markup for a list of posts based on query parameters and template files.
 */
function visceral_query_shortcode($atts)
{
    extract(shortcode_atts(array(
        'container_class' => '',
        'template' => 'views/partials/list-item',
        'show_pagination' => 0,
        'no_posts' => '',
        'post_type' => 'post',
        'posts_per_page' => 6,
        'offset' => 0,
        'order' => 'DESC',
        'orderby' => 'post_date',
        'post__not_in' => '',
        'taxonomy' => '',
        'terms' => ''
    ), $atts));

    $markup = '';

    // Return either a single post type or an array of post types depending on how many were passed in.
    // Note: We need to do this because the Intuitive Custom Post Type plugin won't work with an array of a single post type.
    $post_type = (strpos($post_type, ',') > 0) ? explode(', ', $post_type) : $post_type;

    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => $posts_per_page,
        'offset' => $offset,
        'order' => $order,
        'orderby' => $orderby,
        'post__not_in' => explode(', ', $post__not_in),
    );
    if ($taxonomy && $terms) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    => explode(', ', $terms),
            ),
        );
    }
    $query = new \WP_Query($args);

    if ($query->posts) :
        $markup .= '<div class="' . $container_class . '">';
        foreach ($query->posts as $post) :
            $post = get_object_vars($post);
            ob_start();
            include \App\template_path(locate_template($template . '-' . $post_type . '.blade.php'), array('test' => 'wow'));
            $markup .= ob_get_contents();
            ob_end_clean();
        endforeach;
        $markup .= '</div>';
        wp_reset_postdata();

        // Pagination
        $total = $query->max_num_pages;
        // only bother with pagination if we have more than 1 page
        if ($show_pagination && $total > 1) :
            $big = 999999999; // need an unlikely integer
            $current_page = max(1, get_query_var('paged'));
            $pagination = paginate_links(array(
                'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format'    => '?paged=%#%',
                'current'   => $current_page,
                'total'     => $total,
                'type'      => 'plain',
                'prev_next' => true,
                'prev_text' => '<span class="icon-angle-left">' . __('Previous', 'visceral') . '</span>',
                'next_text' => '<span class="icon-angle-right">' . __('Next', 'visceral') . '</span>'
            ));

            $markup .= '<nav class="pagination text-center">';
            $markup .= $pagination;
            $markup .= '</nav>';
        endif;
    else :
        $markup = $no_posts;
    endif;

    return $markup;
}
add_shortcode('visceral_query', __NAMESPACE__ . '\\visceral_query_shortcode');

add_shortcode('homepage_slides', function () {
    $markup = '';
    $post_fields = App::postFields();
    if (!empty($post_fields['homepage_slides'])) {
        $markup .= '<div class="homepage-slides">';
        $markup .= '<div class="row">';
        foreach ($post_fields['homepage_slides'] as $home_slide) {
            $markup .= '<div class="column xs-100">';
            $markup .= '<div class="homepage-slide text-white overlay" style="background-image: url(' . $home_slide['image']['sizes']['large'] . ');">';
            $markup .= '<div class="homepage-slide__content">';
            $markup .= $home_slide['content'];
            $markup .= '</div>';
            $markup .= '</div>';
            $markup .= '</div>';
        }
        $markup .= '</div>';
        $markup .= '</div>';
    }

    return $markup;
});

add_shortcode('featured_posts', function () {
    $markup = '';
    if (function_exists('get_field')) {
        $posts = get_field('featured_content');
    }
    if ($posts) {
        $markup .= '<div class="row">';
        foreach ($posts as $post) {
            $post_date = get_the_date('F j, Y', $post->ID);
            $post_label = App::postLabels($post->ID);
            if (get_post_type($post->ID) === 'event') {
                $post_label = 'Event';
                if (function_exists('get_field')) {
                    $post_date = get_field('start_date', $post->ID);
                    if (get_field('end_date', $post->ID)) {
                        $post_date .= " - " . get_field('end_date', $post->ID);
                    }
                }
            }

            if (get_field('external_link', $post->ID)) {
                $link = get_field('external_link', $post->ID);
                $icon = '<span class="icon-offsite" aria-hidden="true"></span></a>';
            } elseif (get_field('publication_url', $post->ID)) {
                $link = (get_field('publication_url', $post->ID));
                $icon = '<span class="icon-download" aria-hidden="true"></span></a>';
            } else {
                $link = get_permalink($post->ID);
                $icon = '';
            }

            $markup .= '<div class="column xs-100 md-33">';
            $markup .= '<div class="list-item-featured-post">';
            $markup .= '<div class="list-item-featured-post__content">';
            $markup .= '<p class="post-label">' . $post_label . '</p>';
            $markup .= '<h2 class="list-item-featured-post__title h4"><a href="' . $link . '"><span>' . get_the_title($post->ID) . '</span>' . $icon . '</a></h2>';
            $markup .= '<p class="list-item-featured-post__excerpt">' . wp_trim_words(get_the_excerpt($post->ID), 20) . '</p>';
            $markup .= '<p class="list-item-featured-post__date small">' . $post_date . '</p>';
            $markup .= '</div>';
            $markup .= '</div>';
            $markup .= '</div>';
        }
        $markup .= '</div>';
    }
    return $markup;
});

add_shortcode('recent_news', function () {
    $markup = '';
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3
    );

    $recent_news = new \WP_Query($args);

    if ($recent_news) {
        $markup .= '<section class="recent-posts">
        <div class="container">
            <div class="recent-posts__heading h5">' . __('Recent News &amp; Insights', 'visceral') . '</div>
            <div class="row">';
        while ($recent_news->have_posts()) : $recent_news->the_post();
            ob_start();
            include \App\template_path(locate_template('views/partials/list-item-related-post.blade.php'));
            $markup .= ob_get_contents();
            ob_end_clean();
        endwhile;
        wp_reset_postdata();
        $markup .= '</div>
            </div>
        </section>';
    }
    return $markup;
});

add_shortcode('gift-calculator', function () {
    $markup = '';
    $markup .= '<div class="gift-calculator">';
    $markup .= '<form class="gift-calculator__form" action="">';
    $markup .= '<label for="gift-calculation" class="screen-reader-text">Calculate</label>';
    $markup .= '<input type="text" name="gift-calculation" id="gift-calculation" placeholder="Enter Dollar Amount">';
    $markup .= '<i class="gift-calculator__icon">$</i>';
    $markup .= '<button type="submit" class="screen-reader-text">Submit</button>';
    $markup .= '</form>';
    $markup .= '<p class="gift-calculator__output"></p>';
    $markup .= '</div>';

    return $markup;
});

/* END SHORTCODES */

/* GENERAL PURPOSE FUNCTIONS */

/**
 * RELATED POSTS FUNCTION - Creates an array of related post objects
 *
 * @param int   $total_posts                 Total posts to display
 * @param mixed $post_type                   Either a string or array of post types to display
 * @param string $related_posts_field_name   Name of the manually selected related posts ACF field
 *
 * @return array                             Returns an of related posts
 */
function visceral_related_posts($posts_per_page = 3, $post_type = 'post', $related_posts_field_name = 'related_posts')
{
    // Set up the an array to hold them
    $related_posts = array();
    $selected_posts_ids = array();
    // Get post ID's from related field
    if (function_exists('get_field')) {
        $selected_posts = get_field($related_posts_field_name);
    }
    // Add each post object to our array
    // Subtract each post from total posts
    if ($selected_posts) {
        foreach ($selected_posts as $post_object) {
            array_push($related_posts, $post_object);
            array_push($selected_posts_ids, $post_object->ID);
            $posts_per_page--;
        }
    }

    // If we need more posts, let's get some from recent posts
    if ($posts_per_page) {
        // Don't repeat the posts that we already have
        $exclude_posts = $selected_posts_ids;
        array_push($exclude_posts, get_the_id());

        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => $posts_per_page,
            'exclude' => $exclude_posts,
        );
        $even_more_posts = get_posts($args);
        foreach ($even_more_posts as $post_object) {
            array_push($related_posts, $post_object);
        }
    }


    return $related_posts;
}
// END RELATED POSTS FUNCTION


/**
 * Takes a string and returns a truncated version. Also strips out shortcodes
 *
 * @param  string  $text   String to truncate
 * @param  integer $length Character count limit for truncation
 * @param  string  $append Appended to the end of the string if character count exceeds limit
 * @return string          Truncated string
 */
function truncate_text($text = '', $length = 150, $append = '...')
{
    $new_text = preg_replace(" ([.*?])", '', $text);
    $new_text = strip_shortcodes($new_text);
    $new_text = strip_tags($new_text);
    $new_text = substr($new_text, 0, $length);
    if (strlen($new_text) == $length) {
        $new_text = substr($new_text, 0, strripos($new_text, " "));
        $new_text = $new_text . $append;
    }

    return $new_text;
}

/**
 * Returns an image with a class for fitting to a certain aspect ratio
 *
 * @param  integer $aspect_ratio_width   Aspect Ratio Width
 * @param  integer $aspect_ratio_height   Aspect Ratio Height
 * @param  string  $image_size   Size of featured image to return
 * @return string  markup of image
 */
function get_aspect_ratio_image($id, $aspect_ratio_width = 1, $aspect_ratio_height = 1, $image_size = 'medium')
{
    if (is_null($id)) {
        $id = get_the_id();
    }

    $image = wp_get_attachment_image_src(get_post_thumbnail_id($id), $image_size);
    $w     = $image[1];
    $h     = $image[2];
    $class = ((isset($h) && isset($w)) && ($h / $w) > ($aspect_ratio_height / $aspect_ratio_width)) ? 'portrait' : '';

    return get_the_post_thumbnail($id, $image_size, array('class' => $class));
}

// checks if page is top level
function is_top_level_page($id)
{
    if ($id = null) {
        $id = get_the_id();
    }
    $page = get_post($id);

    if ($page->post_parent === 0) {
        return true;
    } else {
        return false;
    }
}


/**
 * Walker Texas Ranger
 * Inserts some BEM naming sensibility into Wordpress menus
 */

class walker_texas_ranger extends \Walker_Nav_Menu
{

    function __construct($css_class_prefix)
    {

        $this->css_class_prefix = $css_class_prefix;

        // Define menu item names appropriately

        $this->item_css_class_suffixes = array(
            'item'                      => '__item',
            'parent_item'               => '__item--parent',
            'active_item'               => '__item--active',
            'parent_of_active_item'     => '__item--parent--active',
            'ancestor_of_active_item'   => '__item--ancestor--active',
            'sub_menu'                  => '__sub-menu',
            'sub_menu_item'             => '__sub-menu__item',
            'link'                      => '__link',
        );
    }

    public $resource_sub_item = false;

    // Check for children

    function display_element($element, &$children_elements, $max_depth, $depth, $args, &$output)
    {

        $id_field = $this->db_fields['id'];

        if (is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    function start_lvl(&$output, $depth = 1, $args = array())
    {

        $real_depth = $depth + 1;

        $indent = str_repeat("\t", $real_depth);

        $prefix = $this->css_class_prefix;
        $suffix = $this->item_css_class_suffixes;

        $classes = array(
            $prefix . $suffix['sub_menu'],
            $prefix . $suffix['sub_menu'] . '--' . $real_depth
        );


        $class_names = implode(' ', $classes);

        // Add a ul wrapper to sub nav

        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";

        // Adds container and row for mega nav
        if (0 === $depth) {
            $output .= '<div class="container">';

            if ($this->resource_sub_item) {
                $output .= '<div class="row">';
            }
        }
    }
    // Add main/sub classes to li's and links


    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        global $wp_query;

        $indent = ($depth > 0 ? str_repeat("    ", $depth) : ''); // code indent

        $prefix = $this->css_class_prefix;
        $suffix = $this->item_css_class_suffixes;
        $item_classes = $item->classes;

        if ($depth === 0) {
            if (in_array('resources-sub-item-parent', $item_classes)) {
                $this->resource_sub_item = true;
            } else {
                $this->resource_sub_item = false;
            }
        }

        // Item classes
        $item_classes =  array(
            'item_class'            => $depth == 0 ? $prefix . $suffix['item'] : '',
            'parent_class'          => $args->has_children ? $parent_class = $prefix . $suffix['parent_item'] : '',
            'active_page_class'     => in_array("current-menu-item", $item->classes) ? $prefix . $suffix['active_item'] : '',
            'active_parent_class'   => in_array("current-menu-parent", $item->classes) ? $prefix . $suffix['parent_of_active_item'] : '',
            'active_ancestor_class' => in_array("current-menu-ancestor", $item->classes) ? $prefix . $suffix['ancestor_of_active_item'] : '',
            'depth_class'           => $depth >= 1 ? $prefix . $suffix['sub_menu_item'] . ' ' . $prefix . $suffix['sub_menu'] . '--' . $depth . '__item' : '',
            'item_id_class'         => $prefix . '__item--' . $item->object_id,
            // 'user_class'            => $item->classes[0] !== '' ? $prefix . '__item--'. $item->classes[0] : ''
        );

        if (count($item->classes) > 1) {
            foreach ($item->classes as $item_class) {
                $item_classes[] = $prefix . '__item--' . $item_class;
            }
        } else {
            $item_classes['user_class'] = $item->classes[0] !== '' ? $prefix . '__item--' . $item->classes[0] : '';
        }



        // convert array to string excluding any empty values
        $class_string = implode("  ", array_filter($item_classes));

        // Adds column if a mega nav sub-item (first depth)
        if ($depth === 1) {
            if (in_array('main-menu__item--main-item', $item_classes)) {
                if (!in_array('main-menu__item--resources-sub-item', $item_classes)) {
                    $class_string .= ' row';
                }
            }
        }


        // Add the classes to the wrapping <li>
        $output .= $indent . '<li class="' . $class_string . '">';

        // Link classes
        $link_classes = array(
            'item_link'             => $depth == 0 ? $prefix . $suffix['link'] : '',
            'depth_class'           => $depth >= 1 ? $prefix . $suffix['sub_menu'] . $suffix['link'] . '  ' . $prefix . $suffix['sub_menu'] . '--' . $depth . $suffix['link'] : '',
        );

        $link_class_string = implode("  ", array_filter($link_classes));
        $link_class_output = 'class="' . $link_class_string . '"';

        // link attributes
        $attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';

        // Create link markup
        $item_output = $args->before;
        $item_output .= '<a' . $attributes . ' ' . $link_class_output . '>';
        $item_output .=     $args->link_before;
        if (in_array('main-menu__item--main-item', $item_classes)) {
            $item_output .=     '<span>' . apply_filters('the_title', $item->title, $item->ID) . '</span>';
        } else {
            $item_output .=     apply_filters('the_title', $item->title, $item->ID);
        }
        $item_output .=     $args->link_after;
        $item_output .=     $args->after;
        if ($item->description) {
            $item_output .=     '<span class="' . $prefix . $suffix['link'] . '__description' . '">' . $item->description . '</span>';
        }
        $item_output .= '</a>';

        // Filter <li>

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    // The end of a menu item
    function end_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $output .= '</li>';
        if (in_array('resources-sub-item', $item->classes)) {
            if (function_exists('get_field')) {
                $resources_page = get_page_by_path('resources');
                $featured_resources = get_field('featured_resources', $resources_page->ID);
                if ($featured_resources) {
                    $output .= '<div class="featured-resources-container">';
                    foreach ($featured_resources as $resource) {
                        $link_url = '';
                        if (function_exists('get_field')) {
                            if (get_field('external_url', $resource->ID)) {
                                $link_url = get_field('external_url', $resource->ID);
                            }
                            if (get_field('file', $resource->ID)) {
                                $link_url = get_field('file', $resource->ID);
                            }
                        }

                        $output .= '<div class="list-item-featured-resource">';
                        $output .= '<div class="row">';
                        if (get_the_post_thumbnail($resource->ID, 'medium')) {
                            $output .= '<div class="column xs-100 lg-33">';
                            $output .= '<a href="' . $link_url . '" class="list-item-featured-resource__image">';
                            $output .= get_the_post_thumbnail($resource->ID, 'medium', array('alt' => get_the_title($resource->ID)));
                            $output .= '</a>';
                            $output .= '</div>';
                        }
                        $output .= '<div class="column xs-100 lg-auto">';
                        $output .= '<div class="list-item-featured-resource__main">';
                        $output .= '<span class="list-item-featured-resource__post-label post-label">' . 'Featured Resource' . '</span>';
                        $output .= '<p class="list-item-featured-resource__title"><a href="' . $link_url . '">' . get_the_title($resource->ID) . '</a></p>';
                        $output .= '<p class="list-item-featured-resource__date small">' . get_the_date('F j, Y', $resource->ID) . '</p>';
                        $output .= '</div>';
                        $output .= '</div>';
                        $output .= '</div>';
                        $output .= '</div>';
                    }
                    $output .= '</div>';
                }
            }
        }
    }
}

/**
 * bem_menu returns an instance of the walker_texas_ranger class with the following arguments
 * @param  string $location This must be the same as what is set in wp-admin/settings/menus for menu location.
 * @param  string $css_class_prefix This string will prefix all of the menu's classes, BEM syntax friendly
 * @param  arr/string $css_class_modifiers Provide either a string or array of values to apply extra classes to the <ul> but not the <li's>
 * @return [type]
 */

function bem_menu($location = "main_menu", $css_class_prefix = 'main-menu', $css_class_modifiers = null)
{
    // Check to see if any css modifiers were supplied
    if ($css_class_modifiers) {

        if (is_array($css_class_modifiers)) {
            $modifiers = implode(" ", $css_class_modifiers);
        } elseif (is_string($css_class_modifiers)) {
            $modifiers = $css_class_modifiers;
        }
    } else {
        $modifiers = '';
    }

    $args = array(
        'theme_location'    => $location,
        'container'         => false,
        'items_wrap'        => '<ul class="' . $css_class_prefix . ' ' . $modifiers . '">%3$s</ul>',
        'walker'            => new walker_texas_ranger($css_class_prefix, true)
    );

    if (has_nav_menu($location)) {
        return wp_nav_menu($args);
    } else {
        echo "<p>You need to first define a menu in WP-admin<p>";
    }
}
