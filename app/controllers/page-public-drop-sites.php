<?php

namespace App;

use Sober\Controller\Controller;

class PublicDropSites extends Controller
{
    public function publicDropSiteCounties()
    {
        return get_terms('public_drop_site_county');
    }

    public function publicDropSiteCities()
    {
        return get_terms('public_drop_site_city');
    }

    public function dropSites()
    {
        $form_filters = App::formFilters();
        $args = array(
            'post_type'              => 'public_drop_site',
            'posts_per_page'         => -1,
            'tax_query'              => array(
                'relation' => 'AND',
            ),
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        // Convert object to array to check if empty
        if (!empty((array)$form_filters)) {
            // Loop through each filter value
            foreach ($form_filters as $prop => $val) {
                if ($prop !== 'public_drop_site_search') {
                    if ($val) {
                        $args['tax_query'][] = array(
                            'taxonomy' => $prop,
                            'field'    => 'slug',
                            'terms'    => $val,
                        );
                    }
                }
            }
        }

        if (!empty($form_filters->public_drop_site_search) && class_exists('SWP_Query')) {
            $args['s'] = $form_filters->public_drop_site_search;
            $args['engine'] = 'drop_site_engine';

            $query = new \SWP_Query($args);

            // If no keyword, use regular query 	
        } else {
            $query = new \WP_Query($args);
        }



        // Set iterator for below
        $i = 0;
        // Go through each post from wp query
        foreach ($query->posts as $post_obj) {
            // Convert WP Post objects to arrays
            $posts_array[] = get_object_vars($post_obj);

            // post ID
            $post_id = $post_obj->ID;
            $posts_array[$i]['ID'] = $post_id;

            // Fields
            $posts_array[$i]['fields'] = App::postFields($post_id);

            // Terms
            $counties = get_the_terms($post_id, 'public_drop_site_county');
            $posts_array[$i]['county'] = $counties[0]->name;

            $zips = get_the_terms($post_id, 'public_drop_site_zip');
            $posts_array[$i]['zip'] = $zips[0]->name;

            $cities = get_the_terms($post_id, 'public_drop_site_city');
            $posts_array[$i]['city'] = $cities[0]->name;

            // Increase iterator
            $i++;
        }
        // Set object's posts property to newly created array of posts
        $query->posts = $posts_array;

        return $posts_array;
    }
}
