<?php

namespace App;

use Sober\Controller\Controller;

class SinglePerson extends Controller
{

    public function recentPosts()
    {
        $ID = get_the_id();
        $args = array(
            'post_type'              => 'post',
            'posts_per_page'         => 3,
            'post_status'            => 'publish',
            'meta_query' => array(
                                  array(
                                    'key' => 'author',
                                    'value' => '"' . $ID . '"', 
                                    'compare' => 'LIKE'
                                  )
                                ),
            'order'                  => 'ASC',
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
            );
        $recent_posts = get_posts($args);
        
        return $recent_posts;
    }
}
