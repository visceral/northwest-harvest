<?php

namespace App;

use Sober\Controller\Controller;

class SinglePost extends Controller
{
    public function categories()
    {
        $categories = get_the_terms(get_the_id(), 'category');
        $categories_output = '';

        foreach ($categories as $category) {
            $categories_output .= $category->name . ', ';
            $categories_output = substr($categories_output, 0, -2);
        }

        return $categories_output;
    }

    public function relatedPosts()
    {
        $categories = get_the_terms(get_the_id(), 'category');

        if (!empty($categories)) {
            $post_category = $categories[0]->slug;
        }

        $args =  $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            'category_name' => $post_category,
            'exclude' => get_the_id(),
        );

        $posts = get_posts($args);

        return $posts;
    }
}
