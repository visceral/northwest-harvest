<?php

namespace App;

use Sober\Controller\Controller;

class BoardOfDirectors extends Controller
{
    public function people()
    {
        $args = array(
            'post_type'              => 'person',
            'posts_per_page'         => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'person_type',
                    'field'    => 'slug',
                    'terms'    => 'board-of-directors',
                ),
            ),
            // 'meta_key'   			=> 'last_name',
            // 'orderby'    			=> 'meta_value',
            // 'order'					=> 'ASC',
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );
        $query = new \WP_Query($args);

        // Set iterator for below
        $i = 0;
        // Go through each post from wp query
        foreach ($query->posts as $post_obj) {
            // Convert WP Post objects to arrays
            $posts_array[] = get_object_vars($post_obj);

            // post ID
            $post_id = $post_obj->ID;
            $posts_array[$i]['ID'] = $post_id;

            // board member
            $posts_array[$i]['board'] = true;

            // post image
            $post_image = get_aspect_ratio_image($post_id, 1, 1, 'medium');

            // if (!$post_image) {
            //     $post_image = '<img src="' . asset_path('images/logo-placeholder.png') . '" alt="default-image">';
            // }
            $posts_array[$i]['image'] = $post_image;
            
            // Increase iterator
            $i++;
        }
        // Set object's posts property to newly created array of posts
        $query->posts = $posts_array;

        return $query;
    }
}
