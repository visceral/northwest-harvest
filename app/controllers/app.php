<?php

namespace App;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'visceral');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return __('Search Results', 'visceral');
        }
        if (is_404()) {
            return __('Whoops!', 'visceral');
        }
        return get_the_title();
    }

    public static function formFilters()
    {
        $form_filters = new \stdClass();

        foreach ($_REQUEST as $key => $value) {
            $form_filters->$key = $value;
        }

        return $form_filters;
    }

    public static function postLabels($id = false)
    {
        if (!$id) {
            $id = get_the_ID();
        }
        $categories = get_the_terms($id, 'category');
        $categories_output = '';

        foreach ($categories as $category) {
            $categories_output .= $category->name . ', ';
            $categories_output = substr($categories_output, 0, -2);
        }

        return $categories_output;
    }

    public static function postTopics($id = false)
    {
        if (!$id) {
            $id = get_the_ID();
        }
        $topics = get_the_terms($id, 'topic');
        $topics_output = '';

        foreach ($topics as $topic) {
            $topics_output .= $topic->name . ', ';
            $topics_output = substr($topics_output, 0, -2);
        }

        return $topics_output;
    }

    public static function taxonomyList($ID, $taxonomy)
    {
        $categories = get_the_terms($ID, $taxonomy);
        $categories_output = '';

        foreach ($categories as $category) {
            $categories_output .= $category->name . ', ';
        }

        $categories_output = substr($categories_output, 0, -2);

        return $categories_output;
    }

    public function mastheadImage()
    {
        // Set up variable
        $parent_id           = '';
        $image_id            = '';
        $full_size_image_url = '';
        $placeholder_image_url = '';

        // Start off with a check for custom header images early and get the ID
        if (function_exists('get_field') && get_field('custom_header') != '') {
            $custom_header = get_field('custom_header');
            $image_id      = $custom_header['id'];
        }

        elseif (is_category()) {
            $parent_id = get_page_by_path('news')->ID;
        }
        
        // There are templates some that just need custom images.
        elseif (is_search() || is_404()) {
            // For static headers, we just load the same image for main and placeholder.
            $full_size_image_url = $placeholder_image_url = get_template_directory_uri() . '/dist/images/header-custom.jpg';
        }
        
        // Finally, if nothing else, we check for a featured image and get it's ID
        elseif (has_post_thumbnail()) {
            $image_id = get_post_thumbnail_id();
        }
        
        // Remember the parent ID's we got above? Let's adopt their main image and get the ID
        if ($parent_id != '') {
            if ( function_exists('get_field') && get_field('custom_header', $parent_id) != '') {
                $custom_header = get_field('custom_header', $parent_id);
                $image_id = $custom_header['id'];
            } elseif ( has_post_thumbnail($parent_id)) {
                $image_id = get_post_thumbnail_id($parent_id);
            }
        }
        
        
        // By now, we should have an image ID to use. Let's use it to get some URLs.
        if ( $image_id != '' ) {
            $full_size_image = wp_get_attachment_image_src( $image_id,'full', true);
            $full_size_image_url = $full_size_image[0];
        
            $placeholder_image = wp_get_attachment_image_src( $image_id,'thumbnail', true);
            $placeholder_image_url = $placeholder_image[0];

            $img_class = 'masthead--has-img';
        }

        $image = new \stdClass();

        $image->full_size_image_url   = $full_size_image_url;
        $image->placeholder_image_url = $placeholder_image_url;
        $image->img_class             = $img_class;

        return $image;
    }

    public function socialLinks() {
        // Get values from customizer fields
        // and return as list of links

        $social = array();
        $links  = '';

        $social['facebook'] = get_theme_mod('facebook_url');
        $social['twitter']  = get_theme_mod('twitter_url');
        $social['instagram']  = get_theme_mod('instagram_url');
        $social['vimeo']  = get_theme_mod('vimeo_url');
        $social['youtube']  = get_theme_mod('youtube_url');
        $social['linkedin'] = get_theme_mod('linkedin_url');

        if( $social ) {
            $links = '<ul class="social-links list-inline">';
            foreach( $social as $channel => $link ) {
                if( !empty($link) ) {
                    $links .= '<li><a href="' . $link . '" class="social-links__link social-link--' . $channel . '"><i class="icon-' . $channel . ' icon">' . $channel . '</i></a></li>';
                }
            }
            $links .= '</ul>';
        }

        return $links;
    }

    public function alternativeTitle()
    {
        if (is_home()) {
            if (get_field('alternative_title', get_option('page_for_posts'))) {
                return get_field('alternative_title', get_option('page_for_posts'));
            } else {
                return false;
            }
        } else {
            if (get_post_meta(get_the_id(), 'alternative_title', true)) {
                return get_post_meta(get_the_id(), 'alternative_title', true);
            } else {
                return false;
            }
        }
        
    }

    public static function postFields($post_id = null)
    {
        if (is_null($post_id)) {
            $post_id = get_the_id();
        }
        $fields = false;

        // Check to see if ACF is installed
        if (function_exists('get_fields')) {

            // Get fields from ACF
            $fields = get_fields($post_id);

            // Create Empty fields array
            $post_fields = array();

            foreach ($fields as $name => $value) {
                // If field value is a string, set it
                // if (is_string($value)) {
                    $post_fields[$name] = $value;
                // }
                // // If field value is an array
                // if (is_array($value)) {
                //     // If it's an assoc. array
                //     if (count(array_filter(array_keys($value), 'is_string')) > 0) {
                //         // Get sub names and set them as properties and values
                //         // foreach ($value as $sub_key => $sub_value) {
                //         //     $post_fields[$sub_key] = $sub_value;
                //         // }
                //         $post_fields[$name] = $value;
                //     // Else it's a simple array
                //     } else {
                //         foreach ($value as $sub_value) {
                //             // If the values within this array are arrays, go deeper!
                //             if (is_array($sub_value)) {
                //                 foreach ($sub_value as $sub_sub_name => $sub_sub_value) {
                //                     $post_fields[$sub_sub_name] = $sub_sub_value;
                //                 }
                //             // Otherwise just grab the value and assign it to the top key
                //             } else {
                //                 $post_fields[$name] = $sub_value;
                //             }
                //         }
                //     }
                // }
            }
        }
        return $post_fields;
    }

    public static function postAuthors($post_id = null)
    {
        if (is_null($post_id)) {
            $post_id = get_the_id();
        }
        // Authors
        $authors_output = '';
        // Relationship field
        if (function_exists('get_field')) {
            $authors = get_field('author', $post_id);
            if ($authors) {
                foreach ($authors as $author) {
                    $authors_output .= '<a href="' . esc_url(get_permalink($author->ID)) . '">' . get_the_title($author->ID) . '</a>, ';
                }
            }
        }

        // Custom Authors
        $custom_author = get_post_meta($post_id, 'custom_author', true);

        if ($custom_author) {
            $authors_output .= '<span>' . $custom_author . '</span>, ';
        }

        if ($authors_output) {
            $authors_output = substr($authors_output, 0, -2);
        }

        return $authors_output;
    }


    public static function personTitle($ID)
    {
        $person_title = get_post_meta($ID, 'job_title', 'true');
    }

    public static function emergencyBanner()
    {
        if (function_exists('get_field')) {
            if (get_field('emergency_alert_banner', 'option') && get_field('emergency_enable', 'option')) {
                return get_field('emergency_alert_banner', 'option');
            } else {
                return false;
            }
        }
    }

    public function featuredPost()
    {
        if (function_exists('get_fields') && get_field('featured_post', get_option('page_for_posts'))) {
            return get_field('featured_post', get_option('page_for_posts'));
        }
    }

    public function featuredEvent()
    {
        if (function_exists('get_fields') && get_field('featured_event')) {
            return get_field('featured_event');
        }
    }
}
