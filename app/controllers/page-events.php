<?php

namespace App;

use Sober\Controller\Controller;

class Events extends Controller
{
    public function events()
    {
        $args = array(
            'post_type'              => 'event',
            'posts_per_page'         => -1,
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
            // order by meta value start_date
            'meta_key'               => 'start_date',
            'orderby'                => 'meta_value',

        );
        $query = new \WP_Query($args);

        // Set iterator for below
        $i = 0;
        // Go through each post from wp query
        foreach ($query->posts as $post_obj) {
            // Convert WP Post objects to arrays
            $posts_array[] = get_object_vars($post_obj);

            // post ID
            $post_id = $post_obj->ID;
            $posts_array[$i]['ID'] = $post_id;

            // post URL
            $posts_array[$i]['url'] = get_permalink($post_id);

            // post image
            $post_image = get_aspect_ratio_image(10, 9, 'full', $post_id);

            // if (!$post_image) {
            //     $post_image = '<img src="' . asset_path('images/logo-placeholder.png') . '" alt="default-image">';
            // }
            $posts_array[$i]['image'] = $post_image;
            $posts_array[$i]['post_fields'] = App::postFields($post_id);

            // Increase iterator
            $i++;
        }
        // Set object's posts property to newly created array of posts
        $query->posts = $posts_array;

        return $query;
    }
}
