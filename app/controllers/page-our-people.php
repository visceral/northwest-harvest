<?php

namespace App;

use Sober\Controller\Controller;

class OurPeople extends Controller
{
    public function peopleDepartments()
    {
        return get_terms('person_department');
    }

    public function peopleDepartmentPosts()
    {
        $departments = get_terms('person_department');
        $department_sections = array();
        if (!empty($departments)) {
            foreach ($departments as $department) {
                $department_sections[$department->slug]['title'] = $department->name;
                $department_sections[$department->slug]['posts'] = array();
            }
        }

        $args = array(
            'post_type'              => 'person',
            'posts_per_page'         => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'person_type',
                    'field'    => 'slug',
                    'terms'    => 'staff',
                ),
            ),
            'meta_key'   			=> 'first_name',
            'orderby'    			=> 'meta_value',
            'order'					=> 'ASC',
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );
        $people_query = new \WP_Query($args);

        foreach ($people_query->posts as $person) {
            $person = get_object_vars($person);
            $person['image'] = get_aspect_ratio_image($person['ID'], 1, 1, 'medium');
            $person_departments = get_the_terms($person['ID'], 'person_department');
            $person['senior'] = false;
            // Go through each department
            foreach ($person_departments as $person_department) {
                // Check if leadership team (needed to add link to person post page)
                if ($person_department->slug === 'leadership-team') {
                    $person['senior'] = true;
                }
                // Go through each section
                foreach ($department_sections as $key => $section) {
                    // If a person is in that department section, add them to the section
                    if ($person_department->slug === $key) {
                        array_push($department_sections[$key]['posts'], $person);
                    }
                }
            }
        }

        return $department_sections;
    }
}
