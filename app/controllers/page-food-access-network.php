<?php

namespace App;

use Sober\Controller\Controller;

class FoodAccessNetwork extends Controller
{
    public function networkPrograms()
    {
        return get_terms('network_program');
    }

    public function networkCounties()
    {
        return get_terms('network_location_county');
    }

    public function locations()
    {
        $form_filters = App::formFilters();
        $args = array(
            'post_type'              => 'network_location',
            'posts_per_page'         => -1,
            'tax_query'              => array(
                'relation' => 'AND',
            ),
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        // Convert object to array to check if empty
        if (!empty((array)$form_filters)) {
            // Loop through each filter value
            foreach ($form_filters as $prop => $val) {
                if ($val) {
                    $args['tax_query'][] = array(
                        'taxonomy' => $prop,
                        'field'    => 'slug',
                        'terms'    => $val,
                    );
                }
            }
        }

        $query = new \WP_Query($args);

        // Set iterator for below
        $i = 0;
        // Go through each post from wp query
        foreach ($query->posts as $post_obj) {
            // Convert WP Post objects to arrays
            $posts_array[] = get_object_vars($post_obj);

            // post ID
            $post_id = $post_obj->ID;
            $posts_array[$i]['ID'] = $post_id;

            // Fields
            $posts_array[$i]['fields'] = App::postFields($post_id);

            // Terms
            $terms = get_the_terms($post_id, 'network_program');
            $posts_array[$i]['program'] = $terms[0]->slug;

            // Increase iterator
            $i++;
        }
        // Set object's posts property to newly created array of posts
        $query->posts = $posts_array;

        return $posts_array;
    }
}
