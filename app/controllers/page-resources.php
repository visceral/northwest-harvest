<?php

namespace App;

use Sober\Controller\Controller;

class Resources extends Controller
{
    public function resourceTopics()
    {
        $terms_array = array();
        $terms =  get_terms('resource_topic');

        foreach ($terms as $term) {
            if ($term->parent === 0) {
                $terms_array[] = $term;
                // var_dump(get_term_children($term->ID, 'resource_topic'));
                if (get_term_children($term->term_id, 'resource_topic')) {
                    // Creates an array of the child term objects of the current term
                    $child_terms = get_terms('resource_topic', array('parent' => $term->term_id));
                    foreach ($child_terms as $child_term) {
                        $terms_array[] = $child_term;
                    }
                }
            }
        }

        return $terms_array;
    }

    public function resources()
    {
        $args = array(
            'post_type'              => 'resource',
            'posts_per_page'         => -1,
            'update_post_term_cache' => false, // Improves Query performance
            'update_post_meta_cache' => false, // Improves Query performance
        );

        $form_filters = App::formFilters();

        if ($form_filters->topic) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'resource_topic',
                    'field'    => 'slug',
                    'terms'    => $form_filters->topic,
                )
            );
        }

        if (!empty($form_filters->resource_search) && class_exists('SWP_Query')) {
            $args['s'] = $form_filters->resource_search;
            $args['engine'] = 'resource_engine';

            $query = new \SWP_Query($args);

            // If no keyword, use regular query
        } else {
            $query = new \WP_Query($args);
        }

        // Set iterator for below
        $i = 0;
        // Go through each post from wp query
        foreach ($query->posts as $post_obj) {
            // Convert WP Post objects to arrays
            $posts_array[] = get_object_vars($post_obj);

            // post ID
            $post_id = $post_obj->ID;
            $posts_array[$i]['ID'] = $post_id;

            // post URL
            $posts_array[$i]['url'] = get_permalink($post_id);

            // post image
            $post_image = get_aspect_ratio_image($post_id, 10, 9, 'medium');

            // if (!$post_image) {
            //     $post_image = '<img src="' . asset_path('images/logo-placeholder.png') . '" alt="default-image">';
            // }
            $posts_array[$i]['image'] = $post_image;

            // post fields
            $posts_array[$i]['fields'] = App::postFields($post_id);

            $link_text = 'Download';
            $icon = 'icon-download';
            $link_url = '';

            if ($posts_array[$i]['fields']['external_link']) {
                $link_text = 'View Website';
                $icon = 'icon-offsite';
                $link_url = $posts_array[$i]['fields']['external_link'];
            }

            if ($posts_array[$i]['fields']['file']) {
                $link_url = $posts_array[$i]['fields']['file'];
            }

            $posts_array[$i]['download_link'] = "<a class='list-item-resource__download link--underline' href='{$link_url}'>{$link_text}<span class='{$icon}' aria-hidden='true'></span></a>";

            // Increase iterator
            $i++;
        }
        // Set object's posts property to newly created array of posts
        $query->posts = $posts_array;

        return $query;
    }
}
